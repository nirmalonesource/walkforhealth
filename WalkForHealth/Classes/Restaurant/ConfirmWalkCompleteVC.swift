import UIKit
import GoogleMaps

public protocol ConfirmWalkCompleteDelegate {
    func promocodeConfirmed()
}
class ConfirmWalkCompleteVC: UIViewController {
    //MARK: - Properties
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var viewMap: GMSMapView!
    @IBOutlet weak var labelWalkerName: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelStepCount: UILabel!
    @IBOutlet weak var labelCaloriesBorn: UILabel!
    @IBOutlet weak var labelRequiredCalories: UILabel!
    @IBOutlet weak var buttonFinish: UIButton!
    
    @IBOutlet weak var viewMainSuccess: UIView!
    @IBOutlet weak var viewSuccess: UIView!
    @IBOutlet weak var buttonOK: UIButton!
    
    var delegate: ConfirmWalkCompleteDelegate?
    
    private var arrOfferDetail = [[String: Any]]()
    private var path = GMSMutablePath()
    
    var dictUserData = [String: Any]()
    var strStatus = String()
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !MyModel().isConnectedToInternet() {
            MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET, alertController: self)
        } else {
            getOfferDataAPI()
        }
        
        viewMainSuccess.isHidden = true
        
        let userStatus = dictUserData["UserStatus"] as! String
        if userStatus == "Running" {
            buttonFinish.isHidden = true
        } else {
            buttonFinish.isHidden = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        viewNavigation.backgroundColor = Define.APP_COLOR
        Design().setLabel(label: labelWalkerName)
        Design().setButton(button: buttonFinish)
        
        labelTitle.text = "Walk To \(Define.USERDEFAULT.value(forKey: "RestaurantName") as! String)"
        labelWalkerName.text = dictUserData["Name"] as? String
        labelCaloriesBorn.text = "50"
        labelRequiredCalories.text = "80"
        
        viewSuccess.layer.cornerRadius = 10
    }
    
    override func viewDidAppear(_ animated: Bool) {
        viewMainSuccess.center.y += self.view.frame.height
        viewMainSuccess.isHidden = false
    }
    
    //MARK: - Button Mehtod
    @IBAction func buttonBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonFinish(_ sender: Any) {
        
        if !MyModel().isConnectedToInternet() {
            MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET, alertController: self)
        } else {
            confirmWalkerOfferAPI()
        }
    }
    
    @IBAction func buttonOK(_ sender: Any) {
        self.delegate?.promocodeConfirmed()
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - API
extension ConfirmWalkCompleteVC {
    func getOfferDataAPI() {
        Define.APPDELEGATE.showLoadingView()
        let parameter: [String: Any] = ["WalkerOfferID": dictUserData["WalkerOfferID"]!]
        let strURL = Define.API_BASE_URL + Define.API_USER_OFFER_DETAIL
        print("Parameter: ", parameter, "\nURL: ", strURL)
        SwiftAPI().postGetActiveOfferMethod(stringURL: strURL,
                                            userID: Define.USERDEFAULT.value(forKey: "UserID") as! String,
                                            parameter: parameter,
                                            header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result, error) in
            if let error = error {
                
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.getOfferDataAPI()
            } else {
                
                Define.APPDELEGATE.hideLoadingView()
                print("Result", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    
                    self.arrOfferDetail = result!["data"] as! [[String: Any]]
                    self.setPath()
                    
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
    
    func confirmWalkerOfferAPI() {
        Define.APPDELEGATE.showLoadingView()
        let parameter: [String: Any] = ["WalkerOfferID": dictUserData["WalkerOfferID"]!,
                                        "Status": "Confirmed",
                                        "Remarks": ""]
        let strURL = Define.API_BASE_URL + Define.API_CONFIRM_WALKER_OFFER
        print("Parameter: ", parameter, "\nURL: ", strURL)
        SwiftAPI().postGetActiveOfferMethod(stringURL: strURL,
                                            userID: Define.USERDEFAULT.value(forKey: "UserID") as! String,
                                            parameter: parameter,
                                            header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result, error) in
            if let error = error {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_INTERNET,
//                                           alertController: self)
                self.confirmWalkerOfferAPI()
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as! Int
                if status == 200 {
                    UIView.animate(withDuration: 0.1,
                                   delay: 0.1,
                                   options: [],
                                   animations: {
                                    self.viewMainSuccess.center.y -= self.view.frame.height
                    }) { _ in
                        
                    }
                } else {
                    MyModel().showAlertMessage(alertTitle: "alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
    
    func setPath() {
        if self.arrOfferDetail.count > 0 {
            let dictOfferDetail = arrOfferDetail[0]
            //Set Data
            labelStepCount.text = dictOfferDetail["StepsWalked"] as? String
            if let distance = dictOfferDetail["MinimumKM"] as? String {
                labelDistance.text = "\(distance) KM"
            } else {
                labelDistance.text = "0 KM"
            }
            
            //Set Path
            if let arrData = dictOfferDetail["MapData"] as? [[String: Any]] {
                let arrPath = arrData
                if arrPath.count > 1 {
                    
                    for (index, dictData) in arrPath.enumerated() {
                        let lat = Double(dictData["Latitude"] as! String)
                        let long = Double(dictData["Longitude"] as! String)
                        path.addLatitude(lat!, longitude: long!)
                        let rectangle = GMSPolyline(path: path)
                        rectangle.strokeWidth = 2
                        rectangle.map = viewMap
                        
                        if index == 0 {
                            self.setMap(latitude: lat!, longitude: long!)
                            let position = CLLocationCoordinate2DMake(lat!, long!)
                            let marker = GMSMarker(position: position)
                            marker.icon = #imageLiteral(resourceName: "ic_streetviewicon")
                            marker.map = viewMap
                        } else if index == arrPath.count - 1 {
                            let position = CLLocationCoordinate2DMake(lat!, long!)
                            let marker = GMSMarker(position: position)
                            marker.map = viewMap
                        }
                    }
                    
                } else if arrPath.count == 1 {
                    let dictData = arrPath[0]
                    let lat = Double(dictData["Latitude"] as! String)
                    let long = Double(dictData["Longitude"] as! String)
                    self.setMap(latitude: lat!, longitude: long!)
                    let position = CLLocationCoordinate2DMake(lat!, long!)
                    let marker = GMSMarker(position: position)
                    marker.icon = #imageLiteral(resourceName: "ic_streetviewicon")
                    marker.map = viewMap
                } else {
                    print("There is no path.")
                    
                }
            }
            
        }
    }
    
    func setMap(latitude: Double, longitude: Double) {
        let camera = GMSCameraPosition.camera(withLatitude: latitude,
                                              longitude: longitude,
                                              zoom: 13)
        viewMap.animate(to: camera)
    }
}
