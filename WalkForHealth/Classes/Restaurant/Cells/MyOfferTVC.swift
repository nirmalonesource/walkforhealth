import UIKit

class MyOfferTVC: UITableViewCell {

    @IBOutlet weak var viewOffer: UIView!
    @IBOutlet weak var labelOfferName: UILabel!
    @IBOutlet weak var labelDiscount: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelOfferLimit: UILabel!
    @IBOutlet weak var labelOfferDetail: UILabel!
    @IBOutlet weak var buttonOfferEdit: UIButton!
    @IBOutlet weak var labelOfferLimitation: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Design().viewWithShadow(view: viewOffer)
        Design().setLabel(label: labelOfferName)
        Design().setLabel(label: labelDiscount)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
