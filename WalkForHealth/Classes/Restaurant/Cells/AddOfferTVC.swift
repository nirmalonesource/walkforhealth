import UIKit
//MARK: - Cell
class AddOfferTVC: UITableViewCell {

    //MARK: - Properties
    @IBOutlet weak var textMinKm: UITextField!
    @IBOutlet weak var textDiscount: UITextField!
    @IBOutlet weak var labelMinKm: UILabel!
    @IBOutlet weak var labelDiscount: UILabel!
    @IBOutlet weak var buttonDelete: UIButton!
    
    //MARK: - Default Method.
    override func awakeFromNib() {
        super.awakeFromNib()
        textMinKm.delegate = self
        textDiscount.delegate = self
        labelMinKm.backgroundColor = Define.LABEL_DARK_COLOR
        labelDiscount.backgroundColor = Define.LABEL_DARK_COLOR
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

    
}

//MARK: - textField Delegate method
extension AddOfferTVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == textMinKm {
            labelMinKm.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        } else if textField == textDiscount {
            labelDiscount.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == textMinKm {
            labelMinKm.backgroundColor = Define.LABEL_DARK_COLOR
        } else if textField == textDiscount {
            labelDiscount.backgroundColor = Define.LABEL_DARK_COLOR
        }
    }
}

//MARK: - Class Header
class AddOfferHeaderCell: UITableViewCell {
    
}
