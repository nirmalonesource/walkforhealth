import UIKit
import Parchment

class RestaurantDashBoard: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setUpPageMenu()
        Define.USERDEFAULT.set(true, forKey: "isMyOfferReload")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func setUpPageMenu() {
        let firstViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyOfferVC") as! MyOfferVC
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "UserVC") as! UserVC
        
        let pagingViewController = FixedPagingViewController(viewControllers: [
            firstViewController,
            secondViewController
            ])
        
        pagingViewController.font = UIFont(name: "MyriadHebrew-Regular", size: 17)!
        pagingViewController.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        pagingViewController.selectedTextColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        pagingViewController.indicatorColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        pagingViewController.menuBackgroundColor = Define.APP_COLOR
        pagingViewController.selectedFont = UIFont(name: "MyriadHebrew-Regular", size: 17)!
        pagingViewController.indicatorOptions = .visible(height: 3,
                                                         zIndex: Int.max,
                                                         spacing: UIEdgeInsets.zero,
                                                         insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        pagingViewController.borderOptions = .visible(height: 0,
                                                      zIndex: 0,
                                                      insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        //pagingViewController.menuInteraction = .none
        addChildViewController(pagingViewController)
        view.addSubview(pagingViewController.view)
        pagingViewController.didMove(toParentViewController: self)
        
        pagingViewController.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            pagingViewController.view.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 50),
            pagingViewController.view.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
            pagingViewController.view.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            pagingViewController.view.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor)
            ])
    }
    
    @IBAction func buttonMenu(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    
    
}
