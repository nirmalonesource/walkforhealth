import UIKit

class OfferVC: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var tableOffer: UITableView!
    @IBOutlet weak var viewNoOffer: UIView!
    @IBOutlet weak var buttonAdd: UIButton!
    
    private var arrOffers = [[String: Any]]()
    
    var isRefresh = Bool()
    lazy  var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = Define.APP_COLOR
        return refreshControl
    }()
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()

        tableOffer.rowHeight = UITableViewAutomaticDimension
        viewNoOffer.isHidden = true
        tableOffer.addSubview(refreshControl)
        if !MyModel().isConnectedToInternet() {
            MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET,
                                      alertController: self)
        } else {
            getActiveOffersAPI()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        self.viewNavigation.backgroundColor = Define.APP_COLOR
        buttonAdd.layer.cornerRadius = buttonAdd.frame.height / 2
        buttonAdd.clipsToBounds = true
        if Define.USERDEFAULT.bool(forKey: "isMyOfferReload") {
            if !MyModel().isConnectedToInternet() {
                MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET,
                                          alertController: self)
            } else {
                getActiveOffersAPI()
            }
        }
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        isRefresh = true
        refreshControl.beginRefreshing()
        self.getActiveOffersAPI()
        
    }
    
    //MARK: - Button Method
    @IBAction func buttonBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonAdd(_ sender: Any) {
        let addOfferVC = self.storyboard?.instantiateViewController(withIdentifier: "AddNewOfferVC") as! AddNewOfferVC
        addOfferVC.delegate = self
        addOfferVC.isEditOffer = false
        self.present(addOfferVC, animated: true, completion: {
            
        })
    }
    
    @objc func buttonOfferEdit(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tableOffer)
        let indexPath = tableOffer.indexPathForRow(at: buttonPosition) as IndexPath?
        
        let dictData = arrOffers[indexPath!.row]
        
        let offerID = dictData["OfferID"] as! String
        print("OfferID: ", offerID)
        getOfferDetail(offerID: offerID)
    }
}

//MARK: - TableView Delegate Method
extension OfferVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOffers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let offerCell = tableView.dequeueReusableCell(withIdentifier: "MyOfferTVC") as! MyOfferTVC
        let dictData = arrOffers[indexPath.row]
        
        offerCell.labelOfferName.text = dictData["OfferTitle"] as? String
        offerCell.labelOfferLimit.text = "\(MyModel().dateFormatterStringToString2(date: dictData["StartDate"] as! String)) TO \(MyModel().dateFormatterStringToString2(date: dictData["EndDate"] as! String))"
        offerCell.labelOfferDetail.text = dictData["Description"] as? String
        let arrDiscount = dictData["Kilometer"] as! [[String: Any]]
        
        let strOfferLimit = "Remaining offer \(dictData["RemainingOffer"]!) and Offer limit \(dictData["OfferLimit"]!)."
        offerCell.labelOfferLimitation.text = strOfferLimit
        
        if arrDiscount.count > 0 {
            let dictDiscount = getOffer(arrOffers: arrDiscount)
            offerCell.labelDistance.text = "\(dictDiscount!["MinimumKM"] as! String) KM"
            offerCell.labelDiscount.text = "\(dictDiscount!["Discount"] as! String)%"
        }

        //Set Button Method
        offerCell.buttonOfferEdit.addTarget(self,
                                            action: #selector(self.buttonOfferEdit(_:)),
                                            for: .touchUpInside)
        offerCell.tag = indexPath.row
        
        return offerCell
    }
    
    func getOffer(arrOffers: [[String: Any]]) -> [String: Any]? {
        for dictOffer in arrOffers {
            return dictOffer
        }
        return nil
    }
}

//MARK: - API
extension OfferVC {
    func getActiveOffersAPI() {
        self.showLoadingView()
        let parameter: [String: Any] = ["RestaurantID": Define.USERDEFAULT.value(forKey: "RestaurantID")!]
        let strURL = Define.API_BASE_URL + Define.API_RESTAURANT_ACTIVE_OFFER
        print("Parameter: ", parameter, "\nURL: ", strURL)
        SwiftAPI().postGetActiveOfferMethod(stringURL: strURL,
                                            userID: Define.USERDEFAULT.value(forKey: "UserID")! as! String,
                                            parameter: parameter,
                                            header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result,  error) in
            if let error = error {
                if self.isRefresh {
                    self.isRefresh = false
                    self.refreshControl.endRefreshing()
                }
                self.hideLoadingView()
                print("Error: ", error)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.getActiveOffersAPI()
            } else {
                if self.isRefresh {
                    self.isRefresh = false
                    self.refreshControl.endRefreshing()
                }
                self.hideLoadingView()
                print("Result: ",result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    self.arrOffers = result!["data"] as! [[String: Any]]
                    
                    if self.arrOffers.count > 0 {
                        self.viewNoOffer.isHidden = true
                        self.tableOffer.reloadData()
                    } else {
                        self.viewNoOffer.isHidden = false
                    }
                } else {
                    self.viewNoOffer.isHidden = false
                }
            }
        }
    }
    
    func getOfferDetail(offerID: String) {
        Define.APPDELEGATE.showLoadingView()
        let parameter: [String: Any] = ["OfferID": offerID]
        let strURL = Define.API_BASE_URL + Define.API_GET_OFFER_DETAILS
        print("Parameter: ", parameter, "\nURL: ", strURL)
        
        SwiftAPI().postGetActiveOfferMethod(stringURL: strURL,
                                            userID: Define.USERDEFAULT.value(forKey: "UserID") as! String,
                                            parameter: parameter,
                                            header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result, error) in
            if error != nil {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error!)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.getOfferDetail(offerID: offerID)
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                
                let status = result!["status_code"] as! Int
                if status == 200 {
                    let arrData = result!["data"] as! [[String: Any]]
                    if arrData.count > 0 {
                        let dictData = arrData[0]
                        let addVC = self.storyboard?.instantiateViewController(withIdentifier: "AddNewOfferVC") as! AddNewOfferVC
                        addVC.isEditOffer = true
                        addVC.dictOfferDetail = dictData
                        addVC.delegate = self
                        self.present(addVC, animated: true, completion: nil)
                    }
                    
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
}

//MARK: - Loading View
extension OfferVC {
    func showLoadingView() {
        let view = UIView()
        var activityIndecator = UIActivityIndicatorView()
        
        view.frame = (self.view.bounds)
        view.backgroundColor = UIColor.black
        view.tag = 4444
        view.alpha = 0.5
        
        activityIndecator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        activityIndecator.startAnimating()
        
        view.addSubview(activityIndecator)
        activityIndecator.center = view.center
        self.view.addSubview(view)
    }
    
    func hideLoadingView() {
        for view in (self.view.subviews) {
            if view.tag == 4444{
                view.removeFromSuperview()
            }
        }
    }
}

//MARK: - AddNewOfferDelegate Method
extension OfferVC: AddNewOfferDelegate {
    func addNewOfferMethod() {
        dismiss(animated: true, completion: nil)
        self.getActiveOffersAPI()
    }
}
