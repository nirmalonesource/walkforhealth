import UIKit

public protocol AddOfferDelegate {
    func addOfferMethod()
}

class AddOfferVC: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var tableAddOffers: UITableView!
    @IBOutlet weak var buttonOfferLimit: UIButton!
    @IBOutlet weak var buttonSave: UIButton!
    @IBOutlet weak var buttonAdd: UIButton!
    @IBOutlet weak var textMaxOfferLimit: FloatLabelTextField!
    @IBOutlet weak var labelMaxOfferLimit: UILabel!
    
    @IBOutlet weak var constraintTableOfferHeight: NSLayoutConstraint!
    
    var delegate: AddOfferDelegate?
    
    var dictData = [String: Any]()
    
    var dictOfferData = ["MinKM": "0",
                         "Discount": "0"]
    var arrOffers = [[String: String]]()
    var indexPath: IndexPath? = nil
    
    var dropDown: MyDropDown? = nil
    let arrPeopleLimit = ["1 People",
                          "2 People",
                          "3 People",
                          "4 People",
                          "5 People"]
    private var peopleLimit = 1
    
    var isEditOffer = Bool()
    var dictOfferDetail = [String: Any]()
    
    var isAddNewPlan = Bool()
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()
        tableAddOffers.sectionHeaderHeight = 40
        tableAddOffers.rowHeight = 50
        
        arrOffers.append(dictOfferData)
        
        print("Data: ", dictData)
        
        if isEditOffer {
            arrOffers.removeAll()
            let arrData = dictOfferDetail["Kilometer"] as! [[String: Any]]
            for dictData in arrData {
                let strMinKM = dictData["MinimumKM"] as! String
                let strDiscount = dictData["Discount"] as! String
                
                dictOfferData["MinKM"] = strMinKM
                dictOfferData["Discount"] = strDiscount
                
                arrOffers.append(dictOfferData)
            }
            tableAddOffers.reloadData()
            constraintTableOfferHeight.constant = CGFloat(40 + (arrOffers.count * 50))
            let strPeopleLimit = dictOfferDetail["PeopleLimit"] as! String
            buttonOfferLimit.setTitle("\(strPeopleLimit) People", for: .normal)
            peopleLimit = Int(strPeopleLimit)!
            textMaxOfferLimit.text = "\(dictOfferDetail["OfferLimit"]!)"
        }
        
        textMaxOfferLimit.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        self.viewNavigation.backgroundColor = Define.APP_COLOR
        constraintTableOfferHeight.constant = CGFloat(40 + (arrOffers.count * 50))
        buttonAdd.layer.cornerRadius = buttonAdd.frame.height / 2
        buttonAdd.clipsToBounds = true
        Design().setFixButton(button: buttonSave)
    }
    
    //MARK: - Button Method
    @IBAction func buttonBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonOfferLimit(_ sender: Any) {
        if dropDown == nil {
            dropDown = MyDropDown()
            dropDown!.delegate = self
            dropDown!.showMyDropDown(sendButton: buttonOfferLimit,
                                     height: 200,
                                     arrayList: arrPeopleLimit,
                                     imageList: nil,
                                     direction: "Up")
        } else {
            dropDown!.hideMyDropDown(sendButton: buttonOfferLimit)
            dropDown = nil
        }
    }
    
    @IBAction func buttonSave(_ sender: Any) {
        for (index,dictOffer) in arrOffers.enumerated() {
            let indexpath = IndexPath(row: index, section: 0)
            let cell = tableAddOffers.cellForRow(at: indexpath) as! AddOfferTVC
            var minKM = dictOffer["MinKM"]
            var discount = dictOffer["Discount"]
            minKM = cell.textMinKm.text!
            discount = cell.textDiscount.text!
            
            if cell.textMinKm.text!.isEmpty {
                MyModel().showTostMessage(alertMessage: "Enter Minimum KM", alertController: self)
                return
            } else if cell.textDiscount.text!.isEmpty {
                MyModel().showTostMessage(alertMessage: "Enter Discount", alertController: self)
                return
            } else {
                guard Double(minKM!) != nil else {
                    MyModel().showTostMessage(alertMessage: "Enter Proper KM", alertController: self)
                    return
                }
                dictOfferData["MinKM"] = minKM
                dictOfferData["Discount"] = discount
                arrOffers[index] = dictOfferData
            }
        }
        if textMaxOfferLimit.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Enter Max Offer Limit", alertController: self)
        } else {
            print("Data: ", arrOffers)
            let imgData = dictData["BannerImage"] as? UIImage
            
            if imgData == nil {
                addOfferWithoutImageAPI()
            } else {
                addOfferAPI()
            }
        }
    }
    
    @objc func buttonDeleteOffer(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tableAddOffers)
        indexPath = tableAddOffers.indexPathForRow(at: buttonPosition) as IndexPath?
        arrOffers.remove(at: indexPath!.row)
        tableAddOffers.deleteRows(at: [self.indexPath!], with: .none)
        constraintTableOfferHeight.constant = CGFloat(40 + (arrOffers.count * 50))
    }
    
    @IBAction func buttonaddNewOffer(_ sender: UIButton) {
        for (index,dictOffer) in arrOffers.enumerated() {
            let indexpath = IndexPath(row: index, section: 0)
            let cell = tableAddOffers.cellForRow(at: indexpath) as! AddOfferTVC
            var minKM = dictOffer["MinKM"]
            var discount = dictOffer["Discount"]
            minKM = cell.textMinKm.text!
            discount = cell.textDiscount.text!
            if cell.textMinKm.text!.isEmpty {
                MyModel().showTostMessage(alertMessage: "Enter Minimum KM", alertController: self)
                return
            } else if cell.textDiscount.text!.isEmpty {
                MyModel().showTostMessage(alertMessage: "Enter Discount", alertController: self)
                return
            } else {
                dictOfferData["MinKM"] = minKM
                dictOfferData["Discount"] = discount
                arrOffers[index] = dictOfferData
            }
        }
        print(arrOffers)
        arrOffers.append(dictOfferData)
        constraintTableOfferHeight.constant = CGFloat(40 + (arrOffers.count * 50))
        isAddNewPlan = true
        tableAddOffers.beginUpdates()
        tableAddOffers.insertRows(at: [IndexPath(row: arrOffers.count - 1, section: 0)], with: .automatic)
        tableAddOffers.endUpdates()
    }
}

//MARK: - Tableview Delegate Method
extension AddOfferVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "AddOfferHeaderCell") as! AddOfferHeaderCell
        
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOffers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddOfferTVC") as! AddOfferTVC
        
        if isEditOffer {
            
            if isAddNewPlan {
                cell.textMinKm.text =  nil
                cell.textDiscount.text = nil
                isAddNewPlan = false
            } else {
                let dictData = arrOffers[indexPath.row]
                cell.textMinKm.text =  String(dictData["MinKM"]!)
                cell.textDiscount.text = String(dictData["Discount"]!)
            }
        }
        cell.buttonDelete.addTarget(self,
                                    action: #selector(self.buttonDeleteOffer(_:)),
                                    for: .touchUpInside)
        return cell
    }
}

//MARK: - DropDown Delegate Method
extension AddOfferVC: MyDropDownDelegate {
    func recievedSelectedValue(name: String) {
        dropDown!.hideMyDropDown(sendButton: buttonOfferLimit)
        dropDown = nil
        self.buttonOfferLimit.setTitle(name, for: .normal)
        peopleLimit = arrPeopleLimit.index(of: name)! + 1
    }
}

//MARK: - API
extension AddOfferVC {
    func addOfferAPI() {
        Define.APPDELEGATE.showLoadingView()
        var parameter: [String: Any] = ["OfferTitle": dictData["OfferName"]!,
                                        "RestaurantID": Define.USERDEFAULT.value(forKey: "RestaurantID")!,
                                        "StartDate": dictData["FromDate"]!,
                                        "EndDate": dictData["ToDate"]!,
                                        "PeopleLimit": peopleLimit,
                                        "Description": dictData["OfferDetail"]!,
                                        "OfferLimit": textMaxOfferLimit.text!]
        
        for (index, dictOffer) in arrOffers.enumerated() {
            parameter["MinKMWalked[\(index)]"] = dictOffer["MinKM"]
            parameter["Discount[\(index)]"] = dictOffer["Discount"]
        }
        if isEditOffer {
            parameter["OfferID"] = dictOfferDetail["OfferID"]!
        }
        let strURL = Define.API_BASE_URL + Define.API_ADD_OFFER
        print("Parameter: ", parameter, "\nURL: ", strURL)
        let imageData = UIImageJPEGRepresentation(dictData["BannerImage"] as! UIImage, 0.8)
        SwiftAPI().postImageUplodOfferAndProfile(stringURL: strURL,
                                       parameters: parameter,
                                       userID: Define.USERDEFAULT.value(forKey: "UserID")! as! String,
                                       header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String,
                                       imageName: "Offer_Image",
                                       imageData: imageData)
        { (result, error) in
            if let error = error {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.addOfferAPI()
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as?  Int ?? 0
                if status == 200 {
                    Define.USERDEFAULT.set(true, forKey: "isMyOfferReload")
                    
                    var strMessage = String()
                    if self.isEditOffer {
                        strMessage = "Offer Updated Successfully"
                    } else {
                        strMessage = "Offer Added Successfully"
                    }
                    
                    let alert = UIAlertController(title: nil,
                                                  message: strMessage,
                                                  preferredStyle: .alert)
                    self.present(alert, animated: true, completion: nil)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                        self.dismiss(animated: true, completion: {
                            self.delegate?.addOfferMethod()
                        })
                    }
                    //Call MyOffer
                    NotificationCenter.default.post(name: NSNotification.Name("OfferAdded"), object: nil)
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
    
    func addOfferWithoutImageAPI() {
        Define.APPDELEGATE.showLoadingView()
        var parameter: [String: Any] = ["OfferTitle": dictData["OfferName"]!,
                                        "RestaurantID": Define.USERDEFAULT.value(forKey: "RestaurantID")!,
                                        "StartDate": dictData["FromDate"]!,
                                        "EndDate": dictData["ToDate"]!,
                                        "PeopleLimit": peopleLimit,
                                        "Description": dictData["OfferDetail"]!,
                                        "OfferID": dictOfferDetail["OfferID"]!,
                                        "OfferLimit": textMaxOfferLimit.text!]
        
        for (index, dictOffer) in arrOffers.enumerated() {
            parameter["MinKMWalked[\(index)]"] = dictOffer["MinKM"]
            parameter["Discount[\(index)]"] = dictOffer["Discount"]
        }
        
        let strURL = Define.API_BASE_URL + Define.API_ADD_OFFER
        print("Parameter: ", parameter, "\nURL: ", strURL)
        
        SwiftAPI().postGetActiveOfferMethod(stringURL: strURL,
                                            userID: Define.USERDEFAULT.value(forKey: "UserID")! as! String,
                                            parameter: parameter,
                                            header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result, error) in
            if let error = error {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.addOfferWithoutImageAPI()
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    Define.USERDEFAULT.set(true, forKey: "isMyOfferReload")
                    
                    var strMessage = String()
                    if self.isEditOffer {
                        strMessage = "Offer Updated Successfully"
                    } else {
                        strMessage = "Offer Added Successfully"
                    }
                    let alert = UIAlertController(title: nil,
                                                  message: strMessage,
                                                  preferredStyle: .alert)
                    self.present(alert, animated: true, completion: nil)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                        self.dismiss(animated: true, completion: {
                            self.delegate?.addOfferMethod()
                        })
                    }
                    //Call MyOffer
                    NotificationCenter.default.post(name: NSNotification.Name("OfferAdded"), object: nil)
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
}

//MARK: - TextView Delegate Method
extension AddOfferVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == textMaxOfferLimit {
            labelMaxOfferLimit.backgroundColor = UIColor.white
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == textMaxOfferLimit {
            labelMaxOfferLimit.backgroundColor = Define.LABEL_DARK_COLOR
        }
    }
}
