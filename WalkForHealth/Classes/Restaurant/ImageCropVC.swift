import IGRPhotoTweaks

import UIKit

class ImageCropVC: IGRPhotoTweakViewController {

    //MARK: - Properties
    @IBOutlet weak var viewAction: UIView!
    @IBOutlet weak var buttonCrop: UIButton!
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCropView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillLayoutSubviews() {
        self.viewAction.backgroundColor = UIColor.clear
    }
    
    fileprivate func setupCropView() {
        self.setCropAspectRect(aspect: "16:9")
        self.lockAspectRatio(true)
    }
    
    //MARK: - Button Method
    @IBAction func buttonCancel(_ sender: Any) {
        self.dismissAction()
    }
    @IBAction func buttonCrop(_ sender: Any) {
        self.cropAction()
    }
    
}
