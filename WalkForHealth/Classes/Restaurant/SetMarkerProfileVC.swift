import UIKit
import GoogleMaps

public protocol SetMarkerProfileDelegate {
    func getLatLong(lat: Double, long: Double)
}
class SetMarkerProfileVC: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var viewMap: GMSMapView!
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var imageMarker: UIImageView!
    @IBOutlet weak var buttonBack: UIButton!
    
    var delegate: SetMarkerProfileDelegate?
    //Location
    var latitude = Double()
    var longitude = Double()
    private var marker: GMSMarker? = nil
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()

        setMap()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        self.viewNavigation.backgroundColor = Define.APP_COLOR
    }
    
    func setMap() {
        let position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 14.0)
        viewMap.camera = camera
        viewMap.delegate = self
        
        
        marker = GMSMarker()
        marker!.icon = #imageLiteral(resourceName: "ic_set_marker")
        marker!.position = position
        marker!.map = viewMap
        
        let circleCenter : CLLocationCoordinate2D  = position
        let circ = GMSCircle(position: circleCenter, radius: 100)
        circ.fillColor = UIColor(red: 0.0, green: 0.7, blue: 0, alpha: 0.1)
        circ.strokeColor = Define.APP_COLOR
        circ.strokeWidth = 2.5;
        circ.map = viewMap
    }
    
    //MARK: - Button Method
    @IBAction func buttonBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonSave(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.delegate?.getLatLong(lat: latitude, long: longitude)
    }
}

extension SetMarkerProfileVC: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        marker = GMSMarker()
        marker!.icon = #imageLiteral(resourceName: "ic_set_marker")
        marker!.position = position.target
        marker!.map = viewMap
        imageMarker.isHidden = true
        
        let circleCenter : CLLocationCoordinate2D  = position.target
        let circ = GMSCircle(position: circleCenter, radius: 100)
        circ.fillColor = UIColor(red: 0.0, green: 0.7, blue: 0, alpha: 0.1)
        circ.strokeColor = Define.APP_COLOR
        circ.strokeWidth = 2.5;
        circ.map = viewMap
        
        print(position.target.latitude, position.target.longitude)
        latitude = position.target.latitude
        longitude = position.target.longitude
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        mapView.clear()
        imageMarker.isHidden = false
    }
}
