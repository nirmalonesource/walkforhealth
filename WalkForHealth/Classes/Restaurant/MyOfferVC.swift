import UIKit

class MyOfferVC: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var tableMyOffer: UITableView!
    @IBOutlet weak var buttonAdd: UIButton!
    @IBOutlet weak var viewNoOffer: UIView!
    
    private var arrOffers = [[String: Any]]()
    
    var isRefresh = Bool()
    lazy  var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = Define.APP_COLOR
        return refreshControl
    }()
    
    var isReloadOffer = true
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(newOfferAdded),
                                               name: NSNotification.Name("OfferAdded"),
                                               object: nil)
        
        tableMyOffer.rowHeight = UITableViewAutomaticDimension
        viewNoOffer.isHidden = true
        self.tableMyOffer.addSubview(refreshControl)
        
        if Define.USERDEFAULT.bool(forKey: "isMyOfferReload") {
            Define.USERDEFAULT.set(false, forKey: "isMyOfferReload")
            if !MyModel().isConnectedToInternet() {
                MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET,
                                          alertController: self)
            } else {
                getActiveOffersAPI()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        buttonAdd.layer.cornerRadius = buttonAdd.frame.height / 2
        buttonAdd.clipsToBounds = true
    }
    
    @objc func newOfferAdded() {
        getActiveOffersAPI()
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        isRefresh = true
        refreshControl.beginRefreshing()
        self.getActiveOffersAPI()
        
    }
    
    //MARK: -Button Method
    @IBAction func buttonAdd(_ sender: Any) {
        let addOfferVC = self.storyboard?.instantiateViewController(withIdentifier: "AddNewOfferVC") as! AddNewOfferVC
        addOfferVC.delegate = self
        addOfferVC.isEditOffer = false
        self.present(addOfferVC, animated: true, completion: {
            self.isReloadOffer = true
        })
    }
}

//MARK: - Table View Delegate Method
extension MyOfferVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOffers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let offerCell = tableView.dequeueReusableCell(withIdentifier: "MyOfferTVC") as! MyOfferTVC
        let dictData = arrOffers[indexPath.row]
        
        offerCell.labelOfferName.text = dictData["OfferTitle"] as? String
        offerCell.labelOfferLimit.text = "\(MyModel().dateFormatterStringToString2(date: dictData["StartDate"] as! String)) TO \(MyModel().dateFormatterStringToString2(date: dictData["EndDate"] as! String))"
        offerCell.labelOfferDetail.text = dictData["Description"] as? String
        let strOfferLimit = "Remaining offer \(dictData["RemainingOffer"]!) and Offer limit \(dictData["OfferLimit"]!)."
        offerCell.labelOfferLimitation.text = strOfferLimit
        let arrDiscount = dictData["Kilometer"] as! [[String: Any]]
        
        if arrDiscount.count > 0 {
            let dictDiscount = getOffer(arrOffers: arrDiscount)
            offerCell.labelDistance.text = "\(dictDiscount!["MinimumKM"] as! String) KM"
            offerCell.labelDiscount.text = "\(dictDiscount!["Discount"] as! String)%"
        }
        
        return offerCell
    }
    
    func getOffer(arrOffers: [[String: Any]]) -> [String: Any]? {
        for dictOffer in arrOffers {
            return dictOffer
        }
        return nil
    }
}

//MARK: - API
extension MyOfferVC {
    func getActiveOffersAPI() {
        Define.APPDELEGATE.showLoadingView()
        let parameter: [String: Any] = ["RestaurantID": Define.USERDEFAULT.value(forKey: "RestaurantID")!]
        let strURL = Define.API_BASE_URL + Define.API_RESTAURANT_ACTIVE_OFFER
        print("Parameter: ", parameter, "\nURL: ", strURL)
        SwiftAPI().postGetActiveOfferMethod(stringURL: strURL,
                                            userID: Define.USERDEFAULT.value(forKey: "UserID")! as! String,
                                            parameter: parameter,
                                            header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result,  error) in
            if let error = error {
                if self.isRefresh {
                    self.isRefresh = false
                    self.refreshControl.endRefreshing()
                }
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.getActiveOffersAPI()
            } else {
                if self.isRefresh {
                    self.isRefresh = false
                    self.refreshControl.endRefreshing()
                }
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ",result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    self.arrOffers = result!["data"] as! [[String: Any]]
                    if self.arrOffers.count > 0 {
                        self.viewNoOffer.isHidden = true
                        self.tableMyOffer.reloadData()
                    } else {
                        self.viewNoOffer.isHidden = false
                    }
                } else {
                    self.viewNoOffer.isHidden = false
                }
            }
        }
    }
}

//MARK: - AddNewOfferDelegate Method
extension MyOfferVC: AddNewOfferDelegate {
    func addNewOfferMethod() {
        dismiss(animated: true, completion: nil)
        self.getActiveOffersAPI()
    }
}
