import UIKit
import SideMenuSwift
import SDWebImage

class RestaurantMenuVC: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var labelUser: UILabel!
    @IBOutlet weak var labelEmailID: UILabel!
    @IBOutlet weak var tableMenu: UITableView!
    
    @IBOutlet weak var selectionMenuTrailingConstraint: NSLayoutConstraint!
    
    var isDarkModeEnable = false
    var themeColor = UIColor.white
    
    let arrMenuItem = ["My Profile",
                       "Change Password",
                       "My Offers",
                       "Redeemed History",
                       "About Us",
                       "Terms & Condition",
                       "Support"]
    let arrImageMenuItem = [#imageLiteral(resourceName: "ic_menu_user"), #imageLiteral(resourceName: "ic_menu_password"), #imageLiteral(resourceName: "ic_menu_offers"), #imageLiteral(resourceName: "ic_menu_offers"), #imageLiteral(resourceName: "ic_menu_aboutus"), #imageLiteral(resourceName: "ic_terms"), #imageLiteral(resourceName: "ic_menu_support")]
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()

        setSideMenu()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Design().setProfileImage(image: imageProfile)
        let strImageURL = Define.USERDEFAULT.value(forKey: "Image") as! String
        print(strImageURL)
        imageProfile.sd_setImage(with: URL(string: strImageURL))
        let strUserName = Define.USERDEFAULT.value(forKey: "PersonName") as? String
        labelUser.text = strUserName
        labelEmailID.text = Define.USERDEFAULT.value(forKey: "EmailID") as? String
    }
    
    func setSideMenu() {
        selectionMenuTrailingConstraint.constant = 0
        let showPlaceTableOnLeft = (SideMenuController.preferences.basic.position == .under) != (SideMenuController.preferences.basic.direction == .right)
        if showPlaceTableOnLeft {
            selectionMenuTrailingConstraint.constant = SideMenuController.preferences.basic.menuWidth - view.frame.width
        }
        
        view.backgroundColor = themeColor
        tableMenu.backgroundColor = themeColor
        SideMenuController.preferences.basic.enableRubberEffectWhenPanning = false;
        setController()
    }
    
    func setController() {
        // set Controller For Navigation
        sideMenuController?.delegate = self as? SideMenuControllerDelegate
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        let showPlaceTableOnLeft = (SideMenuController.preferences.basic.position == .under) != (SideMenuController.preferences.basic.direction == .right)
        selectionMenuTrailingConstraint.constant = showPlaceTableOnLeft ? SideMenuController.preferences.basic.menuWidth - size.width : 0
        view.layoutIfNeeded()
    }
    
    //MARK: - Button Method
    @IBAction func buttonClose(_ sender: Any) {
        sideMenuController?.hideMenu()
    }
    
    @IBAction func buttonLogout(_ sender: Any) {
        if !MyModel().isConnectedToInternet() {
            MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET,
                                      alertController: self)
        } else {
            self.sideMenuController?.hideMenu()
            logout()
        }
    }
    
    //MARK: - Alert Logout.
    func logout() {
        let alert = UIAlertController(title: "Logout", message: "Are you sure you want to logout?", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel)
        let logout = UIAlertAction(title: "Logout", style: .default) { (alert) in
            self.logoutAPI()
        }
        alert.addAction(cancel)
        alert.addAction(logout)
        self.present(alert, animated: true, completion: nil)
    }
    
}

//MARK: - TableViewDelegate Method
extension RestaurantMenuVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenuItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let menuCell = tableView.dequeueReusableCell(withIdentifier: "MenuTVC") as! MenuTVC
        menuCell.imageMenuIcon.image = arrImageMenuItem[indexPath.row]
        menuCell.labelMenuItem.text = arrMenuItem[indexPath.row]
        return menuCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            DispatchQueue.main.async {
                let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "RestaurantProfileNC")
                self.present(profileVC!, animated: true) {
                    self.sideMenuController?.hideMenu()
                }
            }
        } else if indexPath.row == 1 {
            DispatchQueue.main.async {
                let passwordVC = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordNC")
                self.present(passwordVC!, animated: true) {
                    self.sideMenuController?.hideMenu()
                }
            }
        } else if indexPath.row == 2 {
            DispatchQueue.main.async {
                let offerNC = self.storyboard?.instantiateViewController(withIdentifier: "OfferNC")
                self.present(offerNC!, animated: true) {
                    self.sideMenuController?.hideMenu()
                }
            }
        } else if indexPath.row == 3 {
            DispatchQueue.main.async {
                let redeemVC = self.storyboard?.instantiateViewController(withIdentifier: "RedeemHistoryNC")
                self.present(redeemVC!, animated: true) {
                    self.sideMenuController?.hideMenu()
                }
            }
        } else if indexPath.row == 4 {
            DispatchQueue.main.async {
                let aboutVC = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsNC")
                self.present(aboutVC!, animated: true, completion: {
                    self.sideMenuController?.hideMenu()
                })
            }
        } else if indexPath.row == 5 {
            DispatchQueue.main.async {
                let conditionVC = self.storyboard?.instantiateViewController(withIdentifier: "ConditionNC")
                self.present(conditionVC!, animated: true, completion: {
                    self.sideMenuController?.hideMenu()
                })
            }
        } else if indexPath.row == 6 {
            DispatchQueue.main.async {
                let supportVC = self.storyboard?.instantiateViewController(withIdentifier: "SupportUsNC")
                self.present(supportVC!, animated: true, completion: {
                    self.sideMenuController?.hideMenu()
                })
            }
        } else {
            sideMenuController?.hideMenu()
        }
        
    }
}

//MARK: - API
extension RestaurantMenuVC {
    func logoutAPI() {
        Define.APPDELEGATE.showLoadingView()
        let strURL = Define.API_BASE_URL + Define.API_LOGOUT
        print("URL: ", strURL)
        SwiftAPI().postLogoutMethod(stringURL: strURL,
                                    userID: Define.USERDEFAULT.value(forKey: "UserID") as! String,
                                    header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result, error) in
            if let error = error {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.logout()
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    self.dismiss(animated: true, completion: {
                        MyModel().removeRestaurantData()
                    })
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
}
