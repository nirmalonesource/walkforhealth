import UIKit
import GoogleMaps

class SetMarkerVC: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var viewMap: GMSMapView!
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var imageMarker: UIImageView!
    @IBOutlet weak var buttonBack: UIButton!
    
    //Location
    var latitude = Double()
    var longitude = Double()
    private var marker: GMSMarker? = nil
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        buttonBack.isHidden = true
        if let lat = Define.USERDEFAULT.value(forKey: "Latitude") as? String {
            latitude = Double(lat)!
        }
        
        if let Long = Define.USERDEFAULT.value(forKey: "Longitude") as? String {
            longitude = Double(Long)!
        }
        
        setMap()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        self.viewNavigation.backgroundColor = Define.APP_COLOR
    }
    
    func setMap() {
        let position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 14.0)
        viewMap.camera = camera
        viewMap.delegate = self
        
        
        marker = GMSMarker()
        marker!.icon = #imageLiteral(resourceName: "ic_set_marker")
        marker!.position = position
        marker!.map = viewMap
        
        let circleCenter : CLLocationCoordinate2D  = position
        let circ = GMSCircle(position: circleCenter, radius: 100)
        circ.fillColor = UIColor(red: 0.0, green: 0.7, blue: 0, alpha: 0.1)
        circ.strokeColor = Define.APP_COLOR
        circ.strokeWidth = 2.5;
        circ.map = viewMap
    }
    
    //MARK: - Button Method
    @IBAction func buttonBack(_ sender: UIButton) {
        
    }
    
    @IBAction func buttonSave(_ sender: Any) {
        
       if !MyModel().isConnectedToInternet() {
            MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET, alertController: self)
        } else {
            updateLatLongAPI()
        }
    }
}

extension SetMarkerVC: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        marker = GMSMarker()
        marker!.icon = #imageLiteral(resourceName: "ic_set_marker")
        marker!.position = position.target
        marker!.map = viewMap
        imageMarker.isHidden = true
        
        let circleCenter : CLLocationCoordinate2D  = position.target
        let circ = GMSCircle(position: circleCenter, radius: 100)
        circ.fillColor = UIColor(red: 0.0, green: 0.7, blue: 0, alpha: 0.1)
        circ.strokeColor = Define.APP_COLOR
        circ.strokeWidth = 2.5;
        circ.map = viewMap
        
        print(position.target.latitude, position.target.longitude)
        latitude = position.target.latitude
        longitude = position.target.longitude
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        mapView.clear()
        imageMarker.isHidden = false
    }
}

//MARK: - API
extension SetMarkerVC {
    func updateLatLongAPI() {
        Define.APPDELEGATE.showLoadingView()
        let parameter: [String: Any] = ["RestaurantID": Define.USERDEFAULT.value(forKey: "RestaurantID")!,
                                        "Latitude": latitude,
                                        "Longitude": longitude]
        let strURL = Define.API_BASE_URL + Define.API_UPDATE_RESTAURANT_LONGITUDE
        print("Parameter:", parameter, "\nURL: ", strURL)
        SwiftAPI().postGetActiveOfferMethod(stringURL: strURL,
                                            userID: Define.USERDEFAULT.value(forKey: "UserID") as! String,
                                            parameter: parameter,
                                            header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result, error) in
            if let error = error {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.updateLatLongAPI()
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    Define.USERDEFAULT.set(self.latitude, forKey: "Latitude")
                    Define.USERDEFAULT.set(self.longitude, forKey: "Longitude")
                    let alert = UIAlertController(title: nil,
                                                  message: "Location Updated Successfully",
                                                  preferredStyle: .alert)
                    self.present(alert, animated: true, completion: nil)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                        self.dismiss(animated: true, completion: {
                            let storyBoard = UIStoryboard(name: "Restaurant", bundle: nil)
                            let restaurantVC = storyBoard.instantiateViewController(withIdentifier: "RestaurantNC")
                            self.present(restaurantVC, animated: true, completion: {
                                self.navigationController?.popToRootViewController(animated: false)
                            })
                        })
                    }
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
}
