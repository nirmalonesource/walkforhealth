import UIKit
import GooglePlaces
import SDWebImage

class RegisterVC: UIViewController {

    //MARK: Properties.
    @IBOutlet weak var segmentedChoise: UISegmentedControl!
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var imageDemo: UIImageView!
    @IBOutlet weak var buttonCamera: UIButton!
    
    @IBOutlet weak var viewUser: UIView!
    @IBOutlet weak var viewRestaurant: UIView!
    
    @IBOutlet weak var textFirstName: FloatLabelTextField!
    @IBOutlet weak var labelFirstName: UILabel!
    @IBOutlet weak var textLastName: FloatLabelTextField!
    @IBOutlet weak var labelLastName: UILabel!
    @IBOutlet weak var textCity: FloatLabelTextField!
    @IBOutlet weak var labelCity: UILabel!
    
    @IBOutlet weak var textRestaurantName: FloatLabelTextField!
    @IBOutlet weak var labelRestaurantName: UILabel!
    @IBOutlet weak var textOwnerName: FloatLabelTextField!
    @IBOutlet weak var labelOwnerName: UILabel!
    
    var imagePicker: UIImagePickerController? = nil
    lazy var imageTapper: UITapGestureRecognizer = {
        let imageTapper = UITapGestureRecognizer()
        imageTapper.addTarget(self, action: #selector(self.handleTapper(_:)))
        return imageTapper
    }()
    
    var userType = String()
    var registerFrom = String()
    var registerID = String()
    var isSocialRegister = Bool()
    var strEmailID = String()
    
    var dictSocialData = [String: Any]()
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()
        userType = "User"
        viewProfile.addGestureRecognizer(imageTapper)
        
        textFirstName.delegate = self
        textLastName.delegate = self
        textRestaurantName.delegate = self
        textOwnerName.delegate = self
        
        
        if isSocialRegister {
            if userType == "User" {
                textFirstName.text = dictSocialData["FirstName"] as? String ?? ""
                textLastName.text = dictSocialData["LastName"] as? String ?? ""
            } else {
                textOwnerName.text = dictSocialData["FullName"] as? String ?? ""
            }
            
            if registerFrom == "Google" {
                let imageURL = dictSocialData["Image"] as? URL
                if imageURL != nil {
                    imageProfile.sd_setImage(with: imageURL) { (image, error, catchType, url) in
                        self.imageDemo.isHidden = true
                        self.imageProfile.contentMode = .scaleAspectFill
                    }
                }
            } else if registerFrom == "Facebook" {
                let dictImageData = dictSocialData["ImageData"] as! [String: Any]
                let dictData = dictImageData["data"] as! [String: Any]
                
                let strURL = dictData["url"] as? String ?? ""
                let imageURL = URL(string: strURL)
                imageProfile.sd_setImage(with: imageURL) { (image, error, catchType, url) in
                    self.imageDemo.isHidden = true
                    self.imageProfile.contentMode = .scaleAspectFill
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
    }
    
    override func viewWillLayoutSubviews() {
        segmentedChoise.layer.cornerRadius = segmentedChoise.frame.size.height / 2
        segmentedChoise.layer.masksToBounds = true
        segmentedChoise.layer.borderColor = Define.APP_COLOR.cgColor
        segmentedChoise.layer.borderWidth = 1
        segmentedChoise.setTitleTextAttributes([NSAttributedStringKey.font : UIFont(name: "MyriadHebrew-Regular",
                                                                                    size: 15) as Any],
                                               for: .normal)
        //segmentedChoise.setTitleTextAttributes([NSAttributedStringKey.font. : UIColor.lightGray], for: .disabled)
        viewProfile.layer.cornerRadius = viewProfile.frame.size.height / 2
        viewProfile.layer.masksToBounds = true
        Design().setFixButton(button: buttonNext)
        buttonCamera.layer.cornerRadius = buttonCamera.frame.height / 2
        buttonCamera.clipsToBounds = true
        buttonCamera.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        buttonCamera.layer.borderWidth = 1
    }
    
    @objc func handleTapper(_ imageTapper: UITapGestureRecognizer) {
        imagePicker = UIImagePickerController()
        imagePicker!.delegate = self
        chooseOption()
    }
    
    //MARK: - Button Method
    @IBAction func segmentedChoise(_ sender: Any) {
        if segmentedChoise.selectedSegmentIndex == 0 {
            print("User")
            userType = "User"
            buttonCamera.setImage(#imageLiteral(resourceName: "ic_camera"), for: .normal)
            viewUser.isHidden = false
            viewRestaurant.isHidden = true
        } else if segmentedChoise.selectedSegmentIndex == 1 {
            print("Restaurant")
            userType = "Restaurant"
            buttonCamera.setImage(#imageLiteral(resourceName: "ic_shop"), for: .normal)
            viewUser.isHidden = true
            viewRestaurant.isHidden = false
            textOwnerName.text = dictSocialData["FullName"] as? String ?? ""
        }
    }
    
    @IBAction func buttonCity(_ sender: Any) {
        let autoCompleteViewController = GMSAutocompleteViewController()
        autoCompleteViewController.delegate = self
        self.present(autoCompleteViewController, animated: true, completion: nil)
    }
    
    @IBAction func buttonNext(_ sender: Any) {
        
        if userType == "User" {
            if textFirstName.text!.isEmpty {
                MyModel().showTostMessage(alertMessage: "Enter First Name",
                                          alertController: self)
            } else if textFirstName.text!.count < 3 {
                MyModel().showTostMessage(alertMessage: "Enter Proper First Name",
                                          alertController: self)
            } else if textLastName.text!.isEmpty {
                MyModel().showTostMessage(alertMessage: "Enter Last Name",
                                          alertController: self)
            } else if textLastName.text!.count < 3 {
                MyModel().showTostMessage(alertMessage: "Enter Proper Last Name",
                                          alertController: self)
            } else if textCity.text!.isEmpty {
                MyModel().showTostMessage(alertMessage: "Select City",
                                          alertController: self)
            } else {
                
                let dictRegisterData: [String: String] = ["FirstName": textFirstName.text!,
                                                          "LastName": textLastName.text!,
                                                          "City": textCity.text!]
                
                let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "RegisterContinueVC") as! RegisterContinueVC
                if imageProfile.image == nil {
                    nextVC.isProfileImageSet = false
                } else {
                    nextVC.isProfileImageSet = true
                    nextVC.profileImage = imageProfile.image!
                }
                nextVC.dictRegisterData = dictRegisterData
                nextVC.isSocialRegister = isSocialRegister
                nextVC.strEmailID = strEmailID
                nextVC.registerFrom = registerFrom
                nextVC.registerID = registerID
                self.navigationController?.pushViewController(nextVC, animated: true)
            }
        } else if userType == "Restaurant" {
            if textRestaurantName.text!.isEmpty {
                MyModel().showTostMessage(alertMessage: "Enter Restaurant Name",
                                          alertController: self)
            } else if textRestaurantName.text!.count < 3 {
                MyModel().showTostMessage(alertMessage: "Enter Proper Restaurant Name",
                                          alertController: self)
            } else if textOwnerName.text!.isEmpty {
                MyModel().showTostMessage(alertMessage: "Enter Owner Name",
                                          alertController: self)
            } else if textOwnerName.text!.count < 3 {
                MyModel().showTostMessage(alertMessage: "Enter Proper Owner Name",
                                          alertController: self)
            } else {
                let dictRegisterData: [String: String] = ["RestaurantName": textRestaurantName.text!,
                                                          "PersonName": textOwnerName.text!]
                
                let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "RegisterRestaurantVC") as! RegisterRestaurantVC
                if imageProfile.image == nil {
                    nextVC.isProfileImageSet = false
                } else {
                    nextVC.isProfileImageSet = true
                    nextVC.profileImage = imageProfile.image!
                }
                nextVC.dictRegisterData = dictRegisterData
                nextVC.isSocialRegister = isSocialRegister
                nextVC.strEmailID = strEmailID
                nextVC.registerFrom = registerFrom
                nextVC.registerID = registerID
                self.navigationController?.pushViewController(nextVC, animated: true)
            }
        }
    }
    
    @IBAction func buttonCamera(_ sender: Any) {
        imagePicker = UIImagePickerController()
        imagePicker!.delegate = self
        chooseOption()
    }
    @IBAction func buttonLogin(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - Set Profile Method.
extension RegisterVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func chooseOption() {
        let alertController = UIAlertController (title: nil, message: "Select Option", preferredStyle: .actionSheet)
        let cameraOption = UIAlertAction (title: "Camera", style: .default){
            (action: UIAlertAction) in
            self.fromCamera()
        }
        let photosOption = UIAlertAction (title: "Photos", style: .default){
            (action: UIAlertAction) in
            self.fromPhotos()
        }
        let cancelOption = UIAlertAction (title: "Cancel", style: .cancel)
        
        alertController.addAction(cameraOption)
        alertController.addAction(photosOption)
        alertController.addAction(cancelOption)
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            let popPresenter = alertController.popoverPresentationController
            popPresenter?.sourceView = viewProfile
            popPresenter?.sourceRect = viewProfile.bounds
            self.present(alertController, animated: true, completion: nil)
        }else{
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func fromCamera() {
        imagePicker!.allowsEditing = true
        imagePicker!.sourceType = UIImagePickerControllerSourceType.camera
        self.present(imagePicker!, animated: true, completion: nil)
    }
    
    func fromPhotos() {
        imagePicker!.allowsEditing = true
        imagePicker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(imagePicker!, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
        
        imageDemo.isHidden = true
        imageProfile.contentMode = .scaleAspectFill
        imageProfile.image = chosenImage
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

//MARK: - TextField Delegate Method
extension RegisterVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == textFirstName {
            labelFirstName.backgroundColor = UIColor.white
        } else if textField == textLastName {
            labelLastName.backgroundColor = UIColor.white
        } else if textField == textRestaurantName {
            labelRestaurantName.backgroundColor = UIColor.white
        } else if textField == textOwnerName {
            labelOwnerName.backgroundColor = UIColor.white
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == textFirstName {
            labelFirstName.backgroundColor = Define.LABEL_COLOR
        } else if textField == textLastName {
            labelLastName.backgroundColor = Define.LABEL_COLOR
        } else if textField == textRestaurantName {
            labelRestaurantName.backgroundColor = Define.LABEL_COLOR
        } else if textField == textOwnerName {
            labelOwnerName.backgroundColor = Define.LABEL_COLOR
        }
    }
}

//MARK: - Google Delegate Method
extension RegisterVC: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        print("Name : \(place.name)")
        print("Latitude : \(place.coordinate.latitude)")
        print("Longitude : \(place.coordinate.longitude)")
        
        textCity.text = place.name
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error : \(error.localizedDescription)")
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
}
