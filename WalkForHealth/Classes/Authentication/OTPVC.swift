import UIKit
import FirebaseAuth

class OTPVC: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var labelMobileNo: UILabel!
    @IBOutlet weak var buttonSubmit: UIButton!
    @IBOutlet weak var buttonResendCode: UIButton!
    
    @IBOutlet weak var textFirst: UITextField!
    @IBOutlet weak var textSecond: UITextField!
    @IBOutlet weak var textThird: UITextField!
    @IBOutlet weak var textFour: UITextField!
    @IBOutlet weak var textFive: UITextField!
    @IBOutlet weak var textSix: UITextField!
    
    var OTP = Int()
    var userID = Int()
    var mobileNumber = String()
    
    var registerFrom = String()
    var registerID = String()
    var isSocialRegister = Bool()
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()
        setTextField()
        sendOTPRequest()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        Design().setFixButton(button: buttonSubmit)
        labelMobileNo.text = mobileNumber
        
    }
    
    func setTextField() {
        textFirst.delegate = self
        textSecond.delegate = self
        textThird.delegate = self
        textFour.delegate = self
        textFive.delegate = self
        textSix.delegate = self
        
        textFirst.addTarget(self, action: #selector(changeTextField(textField:)), for: .editingChanged)
        textSecond.addTarget(self, action: #selector(changeTextField(textField:)), for: .editingChanged)
        textThird.addTarget(self, action: #selector(changeTextField(textField:)), for: .editingChanged)
        textFour.addTarget(self, action: #selector(changeTextField(textField:)), for: .editingChanged)
        textFive.addTarget(self, action: #selector(changeTextField(textField:)), for: .editingChanged)
        textSix.addTarget(self, action: #selector(changeTextField(textField:)), for: .editingChanged)
    }
    
    @objc func changeTextField(textField: UITextField) {
        if textField.text!.count >= 1 {
            switch textField {
            case textFirst:
                textSecond.becomeFirstResponder()
            case textSecond:
                textThird.becomeFirstResponder()
            case textThird:
                textFour.becomeFirstResponder()
            case textFour:
                textFive.becomeFirstResponder()
            case textFive:
                textSix.becomeFirstResponder()
            case textSix:
                textField.resignFirstResponder()
            default:
                break
            }
        }
    }

    func sendOTPRequest() {
        PhoneAuthProvider.provider().verifyPhoneNumber(mobileNumber, uiDelegate: nil) { (varificationID, error) in
            if let error = error {
                print("Error: ", error)
                return
            }
            // Sign in using the verificationID and the code sent to the user
            print("VarifayID:", varificationID!)
            Define.USERDEFAULT.set(varificationID!, forKey: "varificationID")
            
        }
    }
    
    //MARK: - Button Method.
    @IBAction func buttonBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonSubmit(_ sender: Any) {
        if textFirst.text!.isEmpty || textSecond.text!.isEmpty || textThird.text!.isEmpty || textFour.text!.isEmpty || textFive.text!.isEmpty || textSix.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Enter OTP.", alertController: self)
        } else if !MyModel().isConnectedToInternet() {
            MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET, alertController: self)
        } else {
            Define.APPDELEGATE.showLoadingView()
            let strOTP = "\(textFirst.text!)\(textSecond.text!)\(textThird.text!)\(textFour.text!)\(textFive.text!)\(textSix.text!)"
            let credential = PhoneAuthProvider.provider().credential(withVerificationID: Define.USERDEFAULT.value(forKey: "varificationID") as! String,
                                                                     verificationCode: strOTP)
            Auth.auth().signInAndRetrieveData(with: credential) { (authData, error) in
                Define.APPDELEGATE.hideLoadingView()
                if let error = error {
                    print("Error: ", error)
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: "Wrong OTP",
                                               alertController: self)
                    return
                }

                print("Data: ", authData!.user)
                self.confirmOTPAPI()
            }
        }
    }
    @IBAction func buttonResendCode(_ sender: Any) {
        sendOTPRequest()
    }
}

extension OTPVC: UITextFieldDelegate {
    //MARK: - UITextFieldDelegate Method
    func textFieldDidBeginEditing(_ textField: UITextField) {
       textField.text = nil
    }
}

//MARK: - API
extension OTPVC {
    func confirmOTPAPI() {
        Define.APPDELEGATE.showLoadingView()
        let parameter: [String: Any] = ["UserID": userID,
                                        "DeviceID": Define.USERDEFAULT.value(forKey: "FCMToken")!]
        let strURL = Define.API_BASE_URL + Define.API_OTP
        print("Parameter: ", parameter, "\nURL: ", strURL)
        let Header = "Basic V0FMS0lORy1BRE1JTjpBUElAV0FMS0lORyEjJFdFQiQ="
        
        SwiftAPI().postMethod(stringURL: strURL,
                              parameters: parameter,
                              header: Header)
        { (result, error) in
            if let error = error {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: "Could not connect to the server",
//                                           alertController: self)
                self.confirmOTPAPI()
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    let userData = result!["data"] as! [String: Any]
                    MyModel().setData(userData: userData)
                    let userType = Define.USERDEFAULT.value(forKey: "UserType") as! String
                    if userType == "User" {
                        Define.USERDEFAULT.set(Date(), forKey: "LoginDate")
                        if self.isSocialRegister {
                            if self.registerFrom == "Facebook" {
                                Define.USERDEFAULT.set(true, forKey: "isFacebookLogin")
                                Define.USERDEFAULT.set(self.registerID, forKey: "SocialID")
                            } else if self.registerFrom == "Google" {
                                Define.USERDEFAULT.set(true, forKey: "isGoogleLogin")
                                Define.USERDEFAULT.set(self.registerID, forKey: "SocialID")
                            }
                        }
                        let alert = UIAlertController(title: nil,
                                                      message: "Register Successfully",
                                                      preferredStyle: .alert)
                        self.present(alert, animated: true, completion: nil)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                            self.dismiss(animated: true, completion: {
                                let storyBoard = UIStoryboard(name: "AddHealth", bundle: nil)
                                let healthVC = storyBoard.instantiateViewController(withIdentifier: "AddHealthNC")
                                Define.USERDEFAULT.set(false, forKey: "isFromDashboard")
                                self.present(healthVC, animated: true, completion: {
                                    self.navigationController?.popToRootViewController(animated: false)
                                })
                                
                            })
                        }
                    } else if userType == "Restaurant" {
                        let alert = UIAlertController(title: nil,
                                                      message: "Register Successfully",
                                                      preferredStyle: .alert)
                        self.present(alert, animated: true, completion: nil)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                            self.dismiss(animated: true, completion: {
                                let updateVC = self.storyboard?.instantiateViewController(withIdentifier: "SetMarkerVC") as! SetMarkerVC
                                self.navigationController?.pushViewController(updateVC, animated: true)
                                
                                if self.isSocialRegister {
                                    if self.registerFrom == "Facebook" {
                                        Define.USERDEFAULT.set(true, forKey: "isFacebookLogin")
                                        Define.USERDEFAULT.set(self.registerID, forKey: "SocialID")
                                    } else if self.registerFrom == "Google" {
                                        Define.USERDEFAULT.set(true, forKey: "isGoogleLogin")
                                        Define.USERDEFAULT.set(self.registerID, forKey: "SocialID")
                                    }
                                }
                            })
                        }
                    }
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
}


