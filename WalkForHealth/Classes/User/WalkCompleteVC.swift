import UIKit
import GoogleMaps

class WalkCompleteVC: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var viewMap: GMSMapView!
    @IBOutlet weak var labelWalkerName: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelStepCount: UILabel!
    @IBOutlet weak var labelCaloriesBorn: UILabel!
    @IBOutlet weak var labelRequiredCalories: UILabel!
    @IBOutlet weak var labelPromoCode: UILabel!
    @IBOutlet weak var labelConfirm: UILabel!
    @IBOutlet weak var imageCouponCode: UIImageView!
    @IBOutlet weak var viewActivity: UIView!
    @IBOutlet weak var imageActivity: UIImageView!
    
    var dictOffer = [String: Any]()
    var isFromStepCount = Bool()
    var walkerOfferID = String()
    
    private var arrOfferDetail = [[String: Any]]()
    
    var path = GMSMutablePath()
    private var bounds = GMSCoordinateBounds()
    private var isPromoCodeApply = Bool()
    private var isShowRefresh = Bool()
    private var isPathSet = Bool()
    private var isTimerSet = Bool()
    private var isActivitiSet = Bool()
    
    private var timer = Timer()
    
    let activity = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
    
    var activityIndicator = UIActivityIndicatorView()
    var strLabel = UILabel()
    
    let effectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
    
    var isFromLink = Bool()
    
    //Location
    private var locationMaeneger: CLLocationManager?
    private var latitude = Double()
    private var longitude = Double()
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()
        isShowRefresh = true
        if !MyModel().isConnectedToInternet() {
            MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET,
                                      alertController: self)
        } else {
            getOfferDataAPI()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        viewNavigation.backgroundColor = Define.APP_COLOR
        Design().setLabel(label: labelWalkerName)
        labelWalkerName.text = "\(Define.USERDEFAULT.value(forKey: "FirstName")!) \(Define.USERDEFAULT.value(forKey: "LastName")!)"
        labelCaloriesBorn.text = "50"
        labelRequiredCalories.text = "80"
        
        if !isFromStepCount {
            labelConfirm.isHidden = true
            labelTitle.text = "Walk To \(dictOffer["RestaurantName"] as! String)"
            labelStepCount.text = dictOffer["StepsWalked"] as? String
            labelPromoCode.text = dictOffer["CouponCode"] as? String
        } else {
            labelConfirm.isHidden = false
        }
    }
    
    func setTimer() {
        timer = Timer.scheduledTimer(timeInterval: 10,
                                     target: self,
                                     selector: #selector(self.handleTimer),
                                     userInfo: nil,
                                     repeats: true)
    }
    
    @objc func handleTimer() {
        isShowRefresh = false
        getOfferDataAPI()
    }
    
    func setPormoCode() {
        if arrOfferDetail.count > 0 {
            let dictOfferDetail = arrOfferDetail[0]
            self.labelStepCount.text = dictOfferDetail["StepsWalked"] as? String
            let couponCode = dictOfferDetail["CouponCode"] as! String
            
            if !isActivitiSet {
                imageActivity.addSubview(activity)
                activity.center = imageActivity.center
                activity.startAnimating()
            }
            
            if couponCode.range(of: "-")  != nil {
                isPromoCodeApply = false
                self.labelPromoCode.text = couponCode
                if !isTimerSet {
                    isTimerSet = true
                    self.setTimer()
                }
            } else {
                activity.stopAnimating()
                activity.removeFromSuperview()
                self.labelPromoCode.text = couponCode
                isPromoCodeApply = true
                timer.invalidate()
                labelConfirm.isHidden = true
            }
        }
    }
    
    func activityIndicator(_ title: String) {
        
        strLabel.removeFromSuperview()
        activityIndicator.removeFromSuperview()
        effectView.removeFromSuperview()
        
        strLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 160, height: 46))
        strLabel.text = title
        strLabel.font = .systemFont(ofSize: 14, weight: .medium)
        strLabel.textColor = UIColor(white: 0.9, alpha: 0.7)
        
        effectView.frame = CGRect(x: view.frame.midX - strLabel.frame.width/2, y: view.frame.midY - strLabel.frame.height/2 , width: 160, height: 46)
        effectView.layer.cornerRadius = 15
        effectView.layer.masksToBounds = true
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
        activityIndicator.startAnimating()
        
        effectView.contentView.addSubview(activityIndicator)
        effectView.contentView.addSubview(strLabel)
        viewActivity.addSubview(effectView)
    }
    
    //MARK: - Button Method
    @IBAction func buttonBack(_ sender: UIButton) {
        if isFromStepCount {
            timer.invalidate()
        }
        if isFromLink {
            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
}

//MARK: - API
extension WalkCompleteVC {
    func getOfferDataAPI() {
        if isShowRefresh {
            Define.APPDELEGATE.showLoadingView()
        }
        let parameter: [String: Any] = ["WalkerOfferID": walkerOfferID]
        let strURL = Define.API_BASE_URL + Define.API_USER_OFFER_DETAIL
        print("Parameter: ", parameter, "\nURL: ", strURL)
        SwiftAPI().postGetActiveOfferMethod(stringURL: strURL,
                                            userID: Define.USERDEFAULT.value(forKey: "UserID") as! String,
                                            parameter: parameter,
                                            header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result, error) in
            if let error = error {
                if self.isShowRefresh {
                    Define.APPDELEGATE.hideLoadingView()
                }
                print("Error: ", error)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.getOfferDataAPI()
            } else {
                if self.isShowRefresh {
                    Define.APPDELEGATE.hideLoadingView()
                }
                print("Result", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    self.arrOfferDetail = result!["data"] as! [[String: Any]]
                    
                    if !self.isPathSet {
                        self.setPath()
                    }
                    
                    if self.isFromStepCount {
                        self.setPormoCode()
                    }
                    
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
    
    func setPath() {
        isPathSet = true
        if self.arrOfferDetail.count > 0 {
            let dictOfferDetail = arrOfferDetail[0]
            if let distance = dictOfferDetail["MinimumKM"] as?String {
                self.labelDistance.text = "\(distance) KM"
            }
            if let arrData = dictOfferDetail["MapData"] as? [[String: Any]] {
                let arrPath = arrData
                if arrPath.count > 1 {
                    
                    for (index, dictData) in arrPath.enumerated() {
                        let lat = Double(dictData["Latitude"] as! String)
                        let long = Double(dictData["Longitude"] as! String)
                        path.addLatitude(lat!, longitude: long!)
                        let rectangle = GMSPolyline(path: path)
                        rectangle.strokeWidth = 2
                        rectangle.map = viewMap
                        if index == 0 {
                            self.setMap(latitude: lat!, longitude: long!)
                            let position = CLLocationCoordinate2DMake(lat!, long!)
                            let marker = GMSMarker(position: position)
                            marker.icon = #imageLiteral(resourceName: "ic_streetviewicon")
                            marker.map = viewMap
                        } else if index == arrPath.count - 1 {
                            let position = CLLocationCoordinate2DMake(lat!, long!)
                            let marker = GMSMarker(position: position)
                            marker.map = viewMap
                        }
                    }
                } else if arrPath.count == 1 {
                    let dictData = arrPath[0]
                    let lat = Double(dictData["Latitude"] as! String)
                    let long = Double(dictData["Longitude"] as! String)
                    self.setMap(latitude: lat!, longitude: long!)
                    let position = CLLocationCoordinate2DMake(lat!, long!)
                    let marker = GMSMarker(position: position)
                    marker.icon = #imageLiteral(resourceName: "ic_streetviewicon")
                    marker.map = viewMap
                } else {
                    print("There is no path.")
                    checkLocationSerVices()
                }
            } else {
                checkLocationSerVices()
            }
        } else {
            checkLocationSerVices()
        }
        
    }
    
    func setMap(latitude: Double, longitude: Double) {
        let camera = GMSCameraPosition.camera(withLatitude: latitude,
                                              longitude: longitude,
                                              zoom: 13)
        viewMap.animate(to: camera)
    }
}

//MARK: - Location Delegate Method
extension WalkCompleteVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        latitude = (location?.coordinate.latitude)!
        longitude = (location?.coordinate.longitude)!
        print("Latitude: ", (location?.coordinate.latitude)!,
              "\nLongitude: ", (location?.coordinate.longitude)!)
        
        setMap()
        //currentLocation.position = CLLocationCoordinate2DMake(latitude, longitude)
        //currentLocation.map = viewMap
    }
    
    func checkLocationSerVices() {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                determineMyCurrentLocation()
                createSettingsAlertController(title: "ALert",
                                              message: "To enable access jump to setting.")
            case .authorizedAlways, .authorizedWhenInUse:
                determineMyCurrentLocation()
            }
        } else {
            createSettingsAlertController(title: "ALert", message: "To enable location jump to setting.")
        }
    }
    
    func determineMyCurrentLocation() {
        locationMaeneger = CLLocationManager()
        locationMaeneger!.delegate = self
        locationMaeneger!.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationMaeneger!.requestAlwaysAuthorization()
        locationMaeneger!.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            locationMaeneger!.startUpdatingLocation()
        }
    }
    
    func createSettingsAlertController(title: String, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default) { (UIAlertAction) in
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)! as URL, options: [:], completionHandler: nil)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    //MARK: - Map
    func setMap() {
        viewMap.isMyLocationEnabled = false
        let camera = GMSCameraPosition.camera(withLatitude: latitude,
                                              longitude: longitude,
                                              zoom: 18)
        viewMap.animate(to: camera)
        viewMap.mapType = .normal
        
        let position = CLLocationCoordinate2DMake(latitude, longitude)
        let marker = GMSMarker(position: position)
        marker.icon = #imageLiteral(resourceName: "ic_set_marker")
        marker.map = viewMap
        locationMaeneger?.stopUpdatingLocation()
    }
}
