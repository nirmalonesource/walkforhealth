import UIKit
import GoogleMaps
import CoreMotion
import Alamofire
import GooglePlaces

class StepCountVC: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var viewMap: GMSMapView!
    @IBOutlet weak var viewAction: UIView!
    @IBOutlet weak var buttonClose: UIButton!
    @IBOutlet weak var buttonLocation: UIButton!
    @IBOutlet weak var labelStepCount: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelBetweenDistance: UILabel!
    @IBOutlet weak var labelStepValidate: UIImageView!
    
    //CoreMotion
    private let activityManager = CMMotionActivityManager()
    private let pedoMeter = CMPedometer()
    private let pedoMeterData = CMPedometerData()
    private var shouldStartUpdate: Bool = false
    private var startDate: Date? = nil
    var marker = GMSMarker()
    var startMarker: GMSMarker? = nil
    var endMarker: GMSMarker? = nil
    var path = GMSMutablePath()
    
    //Location
    private var locationMaeneger: CLLocationManager?
    private var latitude = Double()
    private var longitude = Double()
    private var isMapLoad = Bool()
    var startLocation: CLLocation? = nil
    var endLocation: CLLocation? = nil
    private var bounds = GMSCoordinateBounds()
    private var currentLocation = GMSMarker()
    
    var dictRestaurant = [String: Any]()
    var walkerOfferID = Int()
    var strStatus = String()
    var isFromLink = Bool()
    
    private var isAPIFired = Bool()
    private var countStep = String()
    
    private var sourcre = String()
    private var destination = String()
    
    private var arrLatitude: [Double]? = nil
    private var arrLongitude: [Double]? = nil
    
    private var humanHeight = Float()
    private var humanMeasure = Float()
    
    private var timer = Timer()
    //MARK: - Defuat Method
    override func viewDidLoad() {
        super.viewDidLoad()
        //ImageView
        self.labelStepValidate.layer.cornerRadius = labelStepValidate.frame.height / 2
        self.labelStepValidate.clipsToBounds = true
        
        print("Data: ", dictRestaurant)
        setData()
        countStep = "0"
        arrLatitude = [Double]()
        arrLongitude = [Double]()
        currentLocation.icon = #imageLiteral(resourceName: "ic_streetviewicon")
        checkLocationSerVices()
        
        labelStepCount.text = "Step\n(\(0))"
        labelDistance.text = "Distance\n(\(0)KM)"
        labelBetweenDistance.text = "Total Distance\n(\(0)KM)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        self.viewNavigation.backgroundColor = Define.APP_COLOR
        self.buttonClose.layer.cornerRadius = self.buttonClose.frame.height / 2
        self.buttonClose.clipsToBounds = true
        self.buttonLocation.layer.cornerRadius = self.buttonLocation.frame.height / 2
        self.buttonLocation.clipsToBounds = true
        self.labelTitle.text = "Walk To \(dictRestaurant["RestaurantName"] as! String)"
        
        guard let startDate = startDate else { return }
        updateStepsCountLabelUsing(startDate: startDate)
    }
    
    func setData() {
        humanHeight = Float(Define.USERDEFAULT.value(forKey: "Height") as! String)!
        let gender = Define.USERDEFAULT.value(forKey: "Gender") as! String
        if gender == "Male" {
            humanMeasure = 0.415
        } else if gender == "Female" {
            humanMeasure = 0.413
        } else {
            humanMeasure = 0.414
        }
    }
    
    //MARK: - Map
    func setMap() {
        viewMap.isMyLocationEnabled = false
        let camera = GMSCameraPosition.camera(withLatitude: latitude,
                                              longitude: longitude,
                                              zoom: 13)
        viewMap.animate(to: camera)
        //viewMap.delegate = self
    }
    
    func setMarker() {
        let lat = Double(dictRestaurant["Latitude"] as! String)
        let long = Double(dictRestaurant["Longitude"] as! String)
        let position = CLLocationCoordinate2DMake(lat!, long!)
        let marker = GMSMarker(position: position)
        marker.map = viewMap
        
        let currentPosition = CLLocationCoordinate2DMake(latitude, longitude)
        bounds = bounds.includingCoordinate(currentPosition)
        bounds = bounds.includingCoordinate(marker.position)
        let update = GMSCameraUpdate.fit(bounds, withPadding: 60)
        viewMap.animate(with: update)
    }
    
    func checkLocationSerVices() {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                determineMyCurrentLocation()
                createSettingsAlertController(title: "ALert",
                                              message: "To enable access jump to setting.")
            case .authorizedAlways, .authorizedWhenInUse:
                determineMyCurrentLocation()
            }
        } else {
            createSettingsAlertController(title: "ALert", message: "To enable location jump to setting.")
        }
    }
    
    func determineMyCurrentLocation() {
        locationMaeneger = CLLocationManager()
        locationMaeneger!.delegate = self
        locationMaeneger!.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationMaeneger!.requestAlwaysAuthorization()
        locationMaeneger!.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            locationMaeneger!.startUpdatingLocation()
        }
    }
    
    private func startStopStepCount() {
        shouldStartUpdate = !shouldStartUpdate
        shouldStartUpdate ? (onStart()) : (onStop())
    }
    
    //MARK: - Button Method
    @IBAction func buttonBack(_ sender: Any) {
        showConfirmView()
    }
    
    @IBAction func buttonCurrentLocation(_ sender: Any) {
        let camera = GMSCameraPosition.camera(withLatitude: latitude,
                                              longitude: longitude,
                                              zoom: 13)
        viewMap.animate(to: camera)
    }
    
    @IBAction func buttonClose(_ sender: Any) {
        showConfirmView()
    }
    
    func showConfirmView() {
        let confirmView = AlertConfirmationView.instanceFromNib() as! AlertConfirmationView
        confirmView.delegate = self
        self.view.addSubview(confirmView)
        confirmView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            confirmView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            confirmView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
            confirmView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            confirmView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor)
            ])
    }
}

//MARK: - Location Delegate Method
extension StepCountVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        latitude = (location?.coordinate.latitude)!
        longitude = (location?.coordinate.longitude)!
        
        currentLocation.position = CLLocationCoordinate2DMake(latitude, longitude)
        currentLocation.map = viewMap
        if !isMapLoad {
            self.sourcre = "\(latitude),\(longitude)"
            self.setMap()
            getRouteAPI()
            startStopStepCount()
            timer = Timer.scheduledTimer(timeInterval: 10,
                                         target: self,
                                         selector: #selector(self.setTimer),
                                         userInfo: nil,
                                          repeats: true)
            isMapLoad = true
            arrLatitude!.append(latitude)
            arrLongitude!.append(longitude)
            
            strStatus = "Running"
            updateWalkerOfferAPI()
        }
        
        if shouldStartUpdate && startLocation == nil {
            startLocation = location
            endLocation = location
        } else {
            endLocation = location
        }
        
        let lat = Double(dictRestaurant["Latitude"] as! String)
        let long = Double(dictRestaurant["Longitude"] as! String)
        let destination = CLLocation(latitude: lat!, longitude: long!)
        
        if shouldStartUpdate {
            let strWalkDistance = String(format: "%.02f", getDistance(origin: startLocation!, destination: endLocation!))
            self.labelDistance.text = "Distance\n(\(strWalkDistance)KM)"
            
            let strRemainingDistance = String(format: "%.02f", getDistance(origin: startLocation!, destination: destination))
            self.labelBetweenDistance.text = "Total Distance\n(\(strRemainingDistance)KM)"
            
            let latLongDistance = getDistanceInMeter(origin: startLocation!, destination: endLocation!)
            let humanDistance = getHumanDistance(height: humanHeight, measure: humanMeasure)
            
            if arrLatitude!.count > 3 {
                if humanDistance < latLongDistance {
                    labelStepValidate.backgroundColor = UIColor.red
                } else {
                    labelStepValidate.backgroundColor = UIColor.green
                }
            }
        }
        
        let distanceMeter = getDistanceInMeter(origin: endLocation!, destination: destination)
        
        if !isAPIFired && distanceMeter < 100 {
            if !MyModel().isConnectedToInternet() {
                MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET,
                                          alertController: self)
            } else {
                isAPIFired = true
                strStatus = "Finished"
                updateWalkerOfferAPI()
            }
        }
    }
    
    func getDistance(origin: CLLocation, destination: CLLocation) -> Double {
        let distanceInMeter = origin.distance(from: destination)
        let kmDistance = distanceInMeter / 1000
        return kmDistance
    }
    
    func getDistanceInMeter(origin: CLLocation, destination: CLLocation) -> Double {
        let distanceInMeter = origin.distance(from: destination)
        return distanceInMeter
    }
    
    @objc func setTimer() {
        arrLatitude!.append((endLocation?.coordinate.latitude)!)
        arrLongitude!.append((endLocation?.coordinate.longitude)!)
    }
}

extension StepCountVC {
    private func onStart() {
        viewMap.clear()
        startLocation = nil
        startMarker = GMSMarker()
        path.removeAllCoordinates()
        //buttonStartStop.setTitle("Stop", for: .normal)
        startDate = Date()
        checkAuthorizationStatus()
        startUpdating()
    }
    
    private func onStop() {
        //buttonStartStop.setTitle("Start", for: .normal)
        startDate = nil
        stopUpdating()
    }
    
    private func startUpdating() {
        if CMMotionActivityManager.isActivityAvailable() {
            startTrackingActivityType()
        } else {
            MyModel().showTostMessage(alertMessage: "Step Count Not Availavel ", alertController: self)
        }
        
        if CMPedometer.isStepCountingAvailable() {
            startCountingSteps()
        } else {
            MyModel().showTostMessage(alertMessage: "Step Count Not Availavel ", alertController: self)
        }
    }
    
    private func checkAuthorizationStatus() {
        switch CMMotionActivityManager.authorizationStatus() {
        case CMAuthorizationStatus.denied:
            onStop()
            MyModel().showTostMessage(alertMessage: "Step Count Not Availavel ", alertController: self)
        default:break
        }
    }
    
    private func stopUpdating() {
        activityManager.stopActivityUpdates()
        pedoMeter.stopUpdates()
        pedoMeter.stopEventUpdates()
    }
    
    private func on(error: Error) {
        //handle error
    }
    
    private func updateStepsCountLabelUsing(startDate: Date) {
        pedoMeter.queryPedometerData(from: startDate, to: Date()) {
            [weak self] pedometerData, error in
            if let error = error {
                self?.on(error: error)
            } else if let pedometerData = pedometerData {
                DispatchQueue.main.async {
                    let stepCount = String(describing: pedometerData.numberOfSteps)
                    self?.countStep = stepCount
                    self?.labelStepCount.text = "Step\n(\(stepCount))"
                }
            }
        }
    }
    
    private func startTrackingActivityType() {
        activityManager.startActivityUpdates(to: OperationQueue.main) {
            (activity: CMMotionActivity?) in
            guard let activity = activity else { return }
            DispatchQueue.main.async {
                if activity.walking {
                    //self?.labelActivity.text = "Walking"
                } else if activity.stationary {
                    //self?.labelActivity.text = "Stationary"
                } else if activity.running {
                    //self?.labelActivity.text = "Running"
                } else if activity.automotive {
                    //self?.labelActivity.text = "Automotive"
                } else if activity.cycling {
                    //self?.labelActivity.text = "Cycling"
                }
            }
        }
    }
    
    private func startCountingSteps() {
        pedoMeter.startUpdates(from: Date()) {
            [weak self] pedometerData, error in
            guard let pedometerData = pedometerData, error == nil else { return }
            
            DispatchQueue.main.async {
                //self?.labelTotalStep.text = pedometerData.numberOfSteps.stringValue
                let stepCount = String(describing: pedometerData.numberOfSteps)
                self?.countStep = stepCount
                //let distance = Double(truncating: pedometerData.distance!) / 1000
                //self?.labelPedoMeterDistance.text = "Step Distance (\(String(format: "%.02f", distance))KM)"
                self?.labelStepCount.text = "Step\n(\(stepCount))"
            }
        }
    }
    
    private func getDistance() {
        
    }
}

//MARK: - Alert Controller
extension StepCountVC {
    
    func createSettingsAlertController(title: String, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default) { (UIAlertAction) in
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)! as URL, options: [:], completionHandler: nil)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
}

//MARK: - AlertConfirmationView Delegate Method
extension StepCountVC: AlertConfirmationViewDelegate {
    func buttonCancel() {
        
    }
    
    func buttonOK() {
        if !MyModel().isConnectedToInternet() {
            MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET,
                                      alertController: self)
        } else {
            //strStatus = "Terminated"
            //terminateWalkerOfferAPI()
            isAPIFired = true
            strStatus = "Terminated"
            updateWalkerOfferAPI()
        }
    }
}

//MARK: - API
extension StepCountVC {
    func getRouteAPI() {
        destination = "\(dictRestaurant["Latitude"] as! String),\(dictRestaurant["Longitude"] as! String)"
        let stringURL = MyModel().getDirectionsUrl(LatLngOrigin: sourcre, LatLngDestination: destination)
        
        print("URL: ", stringURL)
        Alamofire.request(stringURL).responseJSON{
            response in
            switch response.result {
            case .success:
                let dictresult: Dictionary = response.value as! [String : Any]
                print(dictresult)
                let routes = dictresult["routes"] as! [[String : Any]]
                
                for route in routes {
                    let routeOverviewPolyline = route["overview_polyline"] as! [String: Any]
                    let points = routeOverviewPolyline["points"] as! String
                    let path = GMSPath.init(fromEncodedPath: points)
                    let polyline = GMSPolyline.init(path: path)
                    polyline.strokeColor = UIColor.blue
                    polyline.strokeWidth = 2
                    polyline.map = self.viewMap
                    self.setMarker()
                }
                
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    
    //("Terminated","Finished")*
    func updateWalkerOfferAPI() {
        Define.APPDELEGATE.showLoadingView()
        var parameter: [String: Any] = ["WalkerOfferID": walkerOfferID,
                                        "StepsWalked": countStep,
                                        "UserStatus": strStatus]
        for (index, lat) in arrLatitude!.enumerated() {
            parameter["Latitude[\(index)]"] = lat
        }
        
        for (index, long) in arrLongitude!.enumerated() {
            parameter["Longitude[\(index)]"] = long
        }
        
        let strURL = Define.API_BASE_URL + Define.API_UPDATE_WALKER_OFFER
        print("Perameter: ", parameter, "\nURL: ", strURL)
        
        SwiftAPI().postAddUpdateWalkerOfferMethod(stringURL: strURL,
                                                  userID: Define.USERDEFAULT.value(forKey: "UserID") as! String,
                                                  parameter: parameter,
                                                  header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result, error) in
            if let error = error {
                Define.APPDELEGATE.hideLoadingView()
                print(error)
                self.updateWalkerOfferAPI()
                //self.showAlertMessage(message: Define.ALERT_SERVER)
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    if self.strStatus == "Terminated" {
                        self.timer.invalidate()
                        if self.isFromLink {
                            self.dismiss(animated: true, completion: nil)
                        } else {
                            self.navigationController?.popViewController(animated: true)
                        }
                    } else if self.strStatus != "Running" {
                        self.timer.invalidate()
                        if self.isFromLink {
                            let completeVC = self.storyboard?.instantiateViewController(withIdentifier: "WalkCompleteVC") as! WalkCompleteVC
                            completeVC.isFromStepCount = true
                            completeVC.walkerOfferID = String(self.walkerOfferID)
                            completeVC.isFromLink = true
                            self.present(completeVC, animated: true, completion: nil)
                        } else {
                            let completeVC = self.storyboard?.instantiateViewController(withIdentifier: "WalkCompleteVC") as! WalkCompleteVC
                            completeVC.isFromStepCount = true
                            completeVC.walkerOfferID = String(self.walkerOfferID)
                            completeVC.isFromLink = false
                            self.navigationController?.pushViewController(completeVC, animated: true)
                        }
                    }
                } else {
                    self.showAlertMessage(message: result!["message"] as! String)
                }
            }
        }
    }
    
    func terminateWalkerOfferAPI() {
        print("Step: ", countStep)
        Define.APPDELEGATE.showLoadingView()
        let parameter: [String: Any] = ["WalkerOfferID": walkerOfferID,
                                        "UserStatus": strStatus]
        let strURL = Define.API_BASE_URL + Define.API_TERMINATE_WALKER_OFFER
        print("Parameter: ", parameter, "\nURL: ", strURL)
        
        SwiftAPI().postAddUpdateWalkerOfferMethod(stringURL: strURL,
                                                  userID: Define.USERDEFAULT.value(forKey: "UserID") as! String,
                                                  parameter: parameter,
                                                  header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result, error) in
            if let error = error {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.terminateWalkerOfferAPI()
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    self.timer.invalidate()
                    if self.isFromLink {
                        self.dismiss(animated: true, completion: nil)
                    } else {
                        self.navigationController?.popViewController(animated: true)
                    }
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
}

//MARK: - Get Distance
extension StepCountVC {
    func getHumanDistance(height: Float, measure: Float) -> Double {
        let footDistance = (height * measure) / 100
        let totalDistance = footDistance * Float(countStep)!
        //labelPedoMeterDistance.text = "Step Distance (\(String(format: "%.02f", totalDistance / 1000))KM)"
        let incrementDistance = (totalDistance * 25.0) / 100
        let getDistance = Double(totalDistance + incrementDistance)
        return getDistance
    }
    
    
    func showAlertMessage(message: String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let retry = UIAlertAction(title: "Retry", style: .default) { _ in
            self.updateWalkerOfferAPI()
        }
        alert.addAction(retry)
        self.present(alert, animated: true, completion: nil)
    }
}
