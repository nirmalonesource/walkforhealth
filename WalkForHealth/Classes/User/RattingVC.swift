import UIKit
import SDWebImage

public protocol RattingDelegate {
    func ratedSuccessfull()
}

class RattingVC: UIViewController {
    //MARK: - Properteis
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var viewRetting: UIView!
    @IBOutlet weak var buttonStar1: UIButton!
    @IBOutlet weak var buttonStar2: UIButton!
    @IBOutlet weak var buttonStar3: UIButton!
    @IBOutlet weak var buttonStar4: UIButton!
    @IBOutlet weak var buttonStar5: UIButton!
    
    @IBOutlet weak var labelRete1: UILabel!
    @IBOutlet weak var labelRete2: UILabel!
    @IBOutlet weak var labelRete3: UILabel!
    @IBOutlet weak var labelRete4: UILabel!
    @IBOutlet weak var labelRete5: UILabel!
    
    @IBOutlet weak var buttonLater: UIButton!
    @IBOutlet weak var buttonRateNow: UIButton!
    
    @IBOutlet weak var imageAd: UIImageView!
    var delegate: RattingDelegate?
    
    var dictOffer = [String: Any]()
    
    private var rate = Int()
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()

        setRatting(index: 0)
        self.getAdvertisementListAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        self.viewNavigation.backgroundColor = Define.APP_COLOR
        viewRetting.layer.borderWidth = 1
        viewRetting.layer.borderColor = UIColor.white.cgColor
        viewRetting.layer.cornerRadius = 8
        Design().setButton(button: buttonLater)
        Design().setButton(button: buttonRateNow)
        
    }
    
    //Myriad Hebrew Font names: ["MyriadHebrew-Bold", "MyriadHebrew-BoldIt", "MyriadHebrew-Regular", "MyriadHebrew-It"]
    func setRatting(index: Int) {
        if index == 1 {
            //Button
            buttonStar1.setImage(#imageLiteral(resourceName: "ic_fill_star"), for: .normal)
            buttonStar2.setImage(#imageLiteral(resourceName: "ic_empty_star"), for: .normal)
            buttonStar3.setImage(#imageLiteral(resourceName: "ic_empty_star"), for: .normal)
            buttonStar4.setImage(#imageLiteral(resourceName: "ic_empty_star"), for: .normal)
            buttonStar5.setImage(#imageLiteral(resourceName: "ic_empty_star"), for: .normal)
            
            //Label
            labelRete1.font = UIFont(name: "MyriadHebrew-Bold", size: 15)
            labelRete2.font = UIFont(name: "MyriadHebrew-Regular", size: 15)
            labelRete3.font = UIFont(name: "MyriadHebrew-Regular", size: 15)
            labelRete4.font = UIFont(name: "MyriadHebrew-Regular", size: 15)
            labelRete5.font = UIFont(name: "MyriadHebrew-Regular", size: 15)
            
            rate = 1
        } else if index == 2 {
            //Button
            buttonStar1.setImage(#imageLiteral(resourceName: "ic_fill_star"), for: .normal)
            buttonStar2.setImage(#imageLiteral(resourceName: "ic_fill_star"), for: .normal)
            buttonStar3.setImage(#imageLiteral(resourceName: "ic_empty_star"), for: .normal)
            buttonStar4.setImage(#imageLiteral(resourceName: "ic_empty_star"), for: .normal)
            buttonStar5.setImage(#imageLiteral(resourceName: "ic_empty_star"), for: .normal)
            
            //Label
            labelRete1.font = UIFont(name: "MyriadHebrew-Regular", size: 15)
            labelRete2.font = UIFont(name: "MyriadHebrew-Bold", size: 15)
            labelRete3.font = UIFont(name: "MyriadHebrew-Regular", size: 15)
            labelRete4.font = UIFont(name: "MyriadHebrew-Regular", size: 15)
            labelRete5.font = UIFont(name: "MyriadHebrew-Regular", size: 15)
            
            rate = 2
        } else if index == 3 {
            //Button
            buttonStar1.setImage(#imageLiteral(resourceName: "ic_fill_star"), for: .normal)
            buttonStar2.setImage(#imageLiteral(resourceName: "ic_fill_star"), for: .normal)
            buttonStar3.setImage(#imageLiteral(resourceName: "ic_fill_star"), for: .normal)
            buttonStar4.setImage(#imageLiteral(resourceName: "ic_empty_star"), for: .normal)
            buttonStar5.setImage(#imageLiteral(resourceName: "ic_empty_star"), for: .normal)
            
            //Label
            labelRete1.font = UIFont(name: "MyriadHebrew-Regular", size: 15)
            labelRete2.font = UIFont(name: "MyriadHebrew-Regular", size: 15)
            labelRete3.font = UIFont(name: "MyriadHebrew-Bold", size: 15)
            labelRete4.font = UIFont(name: "MyriadHebrew-Regular", size: 15)
            labelRete5.font = UIFont(name: "MyriadHebrew-Regular", size: 15)
            
            rate = 3
        } else if index == 4 {
            //Button
            buttonStar1.setImage(#imageLiteral(resourceName: "ic_fill_star"), for: .normal)
            buttonStar2.setImage(#imageLiteral(resourceName: "ic_fill_star"), for: .normal)
            buttonStar3.setImage(#imageLiteral(resourceName: "ic_fill_star"), for: .normal)
            buttonStar4.setImage(#imageLiteral(resourceName: "ic_fill_star"), for: .normal)
            buttonStar5.setImage(#imageLiteral(resourceName: "ic_empty_star"), for: .normal)
            
            //Label
            labelRete1.font = UIFont(name: "MyriadHebrew-Regular", size: 15)
            labelRete2.font = UIFont(name: "MyriadHebrew-Regular", size: 15)
            labelRete3.font = UIFont(name: "MyriadHebrew-Regular", size: 15)
            labelRete4.font = UIFont(name: "MyriadHebrew-Bold", size: 15)
            labelRete5.font = UIFont(name: "MyriadHebrew-Regular", size: 15)
            
            rate = 4
        } else if index == 5 {
            //Button
            buttonStar1.setImage(#imageLiteral(resourceName: "ic_fill_star"), for: .normal)
            buttonStar2.setImage(#imageLiteral(resourceName: "ic_fill_star"), for: .normal)
            buttonStar3.setImage(#imageLiteral(resourceName: "ic_fill_star"), for: .normal)
            buttonStar4.setImage(#imageLiteral(resourceName: "ic_fill_star"), for: .normal)
            buttonStar5.setImage(#imageLiteral(resourceName: "ic_fill_star"), for: .normal)
            
            //Label
            labelRete1.font = UIFont(name: "MyriadHebrew-Regular", size: 15)
            labelRete2.font = UIFont(name: "MyriadHebrew-Regular", size: 15)
            labelRete3.font = UIFont(name: "MyriadHebrew-Regular", size: 15)
            labelRete4.font = UIFont(name: "MyriadHebrew-Regular", size: 15)
            labelRete5.font = UIFont(name: "MyriadHebrew-Bold", size: 15)
            
            rate = 5
        } else {
            //Button
            buttonStar1.setImage(#imageLiteral(resourceName: "ic_empty_star"), for: .normal)
            buttonStar2.setImage(#imageLiteral(resourceName: "ic_empty_star"), for: .normal)
            buttonStar3.setImage(#imageLiteral(resourceName: "ic_empty_star"), for: .normal)
            buttonStar4.setImage(#imageLiteral(resourceName: "ic_empty_star"), for: .normal)
            buttonStar5.setImage(#imageLiteral(resourceName: "ic_empty_star"), for: .normal)
            
            //Label
            labelRete1.font = UIFont(name: "MyriadHebrew-Regular", size: 15)
            labelRete2.font = UIFont(name: "MyriadHebrew-Regular", size: 15)
            labelRete3.font = UIFont(name: "MyriadHebrew-Regular", size: 15)
            labelRete4.font = UIFont(name: "MyriadHebrew-Regular", size: 15)
            labelRete5.font = UIFont(name: "MyriadHebrew-Regular", size: 15)
            
            rate = 0
        }
    }
    
    
    //MARK: - Button Mrthod
    @IBAction func buttonBack(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonStar1(_ sender: UIButton) {
        setRatting(index: 1)
    }
    
    @IBAction func buttonStar2(_ sender: UIButton) {
        setRatting(index: 2)
    }
    
    @IBAction func buttonStar3(_ sender: UIButton) {
        setRatting(index: 3)
    }
    
    @IBAction func buttonStar4(_ sender: UIButton) {
        setRatting(index: 4)
    }
    
    @IBAction func buttonStar5(_ sender: UIButton) {
        setRatting(index: 5)
    }
    
    @IBAction func buttonLater(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonRateNow(_ sender: UIButton) {
        if !MyModel().isConnectedToInternet() {
            MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET,
                                      alertController: self)
        } else {
            addRattinAPI()
        }
    }
}

//MARK: - API
extension RattingVC {
    func addRattinAPI() {
        Define.APPDELEGATE.showLoadingView()
        let parameter: [String: Any] = ["RestaurantID": dictOffer["RestaurantID"]!,
                                        "OfferID": dictOffer["OfferID"]!,
                                        "WalkerID": Define.USERDEFAULT.value(forKey: "WalkerID")!,
                                        "Ratings": rate]
        let strURL = Define.API_BASE_URL + Define.API_ADD_RATTING
        print("Parameter: ", parameter, "\nURL: ", strURL)
        SwiftAPI().postAddUpdateWalkerOfferMethod(stringURL: strURL,
                                                  userID: Define.USERDEFAULT.value(forKey: "UserID") as! String,
                                                  parameter: parameter,
                                                  header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result, error) in
            if error != nil {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error!)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_INTERNET,
//                                           alertController: self)
                self.addRattinAPI()
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    MyModel().showTostMessage(alertMessage: "Successfull", alertController: self)
                    self.delegate?.ratedSuccessfull()
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
    
    func getAdvertisementListAPI() {
        let parameter: [String: Any] = ["Type": "Rating Page"]
        let strURL = Define.API_BASE_URL + Define.API_ADVERTISEMENT_LIST
        print("Parameter: ", parameter, "\nURL: ", strURL)
        SwiftAPI().postGetActiveOfferMethod(stringURL: strURL,
                                            userID: Define.USERDEFAULT.value(forKey: "UserID") as! String,
                                            parameter: parameter,
                                            header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result, error) in
            if error != nil {
                print("Error: ", error!)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.getAdvertisementListAPI()
            } else {
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    let dictData = result!["data"] as! [String: Any]
                    let activity = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
                    self.imageAd.addSubview(activity)
                    activity.center = self.imageAd.center
                    activity.startAnimating()
                    self.imageAd.sd_setImage(with: URL(string: dictData["AdvImaPath"] as! String))
                    { (image, error, imageType, url) in
                        activity.stopAnimating()
                        activity.removeFromSuperview()
                    }
                } else {
                    
                }
            }
        }
    }
}
