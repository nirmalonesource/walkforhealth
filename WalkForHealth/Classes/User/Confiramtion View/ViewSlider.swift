import UIKit

class ViewSlider: UIView {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelTips: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 8
        self.clipsToBounds = true
    }
}
