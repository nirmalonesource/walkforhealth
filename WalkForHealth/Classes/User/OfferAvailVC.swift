import UIKit

class OfferAvailVC: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var tableOfferAvail: UITableView!
    
    private var arrOffers = [[String: Any]]()
    
    private var indexpath: IndexPath? = nil
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()
        tableOfferAvail.rowHeight = UITableViewAutomaticDimension
        
        if !MyModel().isConnectedToInternet() {
            MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET,
                                      alertController: self)
        } else {
            self.getHisrotyAPI()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        viewNavigation.backgroundColor = Define.APP_COLOR
        
    }
    
    //MARK: - Button Method
    @IBAction func buttonBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func buttonRateNow(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tableOfferAvail)
        indexpath = tableOfferAvail.indexPathForRow(at: buttonPosition) as IndexPath?
        let dictData = arrOffers[indexpath!.row]
        let offerID = dictData["RestaurantID"] as? String
        if offerID == nil {
            MyModel().showAlertMessage(alertTitle: "Alert",
                                       alertMessage: "Offer Not Available.",
                                       alertController: self)
        } else {
            let rateVC = self.storyboard?.instantiateViewController(withIdentifier: "RattingVC") as! RattingVC
            rateVC.dictOffer = arrOffers[indexpath!.row]
            rateVC.delegate = self
            present(rateVC, animated: true, completion: nil)
        }
    }
    
    func showLoadingView() {
        let view = UIView()
        var activityIndecator = UIActivityIndicatorView()
        
        view.frame = (self.view.bounds)
        view.backgroundColor = UIColor.black
        view.tag = 4444
        view.alpha = 0.5
        
        activityIndecator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        activityIndecator.startAnimating()
        
        view.addSubview(activityIndecator)
        activityIndecator.center = view.center
        self.view.addSubview(view)
    }
    
    func hideLoadingView() {
        for view in (self.view.subviews) {
            if view.tag == 4444{
                view.removeFromSuperview()
            }
        }
    }
}

//MARK: - TableView Delegate Method
extension OfferAvailVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOffers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OfferAvailTVC") as! OfferAvailTVC
        let dictData = arrOffers[indexPath.row]
        cell.labelRestaurantName.text = dictData["RestaurantName"] as? String
        cell.labelOfferName.text = dictData["OfferTitle"] as? String
        
        if let distance = dictData["MinimumKM"] as? String {
            cell.labelDistance.text = "\(distance) KM"
        } else {
            cell.labelDistance.text = "0 KM"
        }
        
        if let discount = dictData["Discount"] as? String {
            cell.labelDiscount.text = "\(discount)%"
        } else {
            cell.labelDiscount.text = "0%"
        }
        
        let userStatus = dictData["UserStatus"] as! String
        
        let strRate = dictData["Ratings"] as! String
        let rate = Double(strRate)
        if rate! < 1.0 {
            if userStatus == "Finished" {
                cell.labelRatting.text = "Rate Now"
                cell.labelRatting.isHidden = false
                cell.buttonRatting.isUserInteractionEnabled = true
                cell.imageRate1.image = #imageLiteral(resourceName: "ic_empty_star")
                cell.imageRate2.image = #imageLiteral(resourceName: "ic_empty_star")
                cell.imageRate3.image = #imageLiteral(resourceName: "ic_empty_star")
                cell.imageRate4.image = #imageLiteral(resourceName: "ic_empty_star")
                cell.imageRate5.image = #imageLiteral(resourceName: "ic_empty_star")
            }
        } else if rate! >= 1.0 && rate! < 2.0 {
            cell.labelRatting.isHidden = false
            cell.labelRatting.text = "All Ready Rated"
            cell.buttonRatting.isUserInteractionEnabled = false
            cell.imageRate1.image = #imageLiteral(resourceName: "ic_fill_star")
            cell.imageRate2.image = #imageLiteral(resourceName: "ic_empty_star")
            cell.imageRate3.image = #imageLiteral(resourceName: "ic_empty_star")
            cell.imageRate4.image = #imageLiteral(resourceName: "ic_empty_star")
            cell.imageRate5.image = #imageLiteral(resourceName: "ic_empty_star")
        } else if rate! >= 2 && rate! < 3 {
            cell.labelRatting.isHidden = false
            cell.labelRatting.text = "All Ready Rated"
            cell.buttonRatting.isUserInteractionEnabled = false
            cell.imageRate1.image = #imageLiteral(resourceName: "ic_fill_star")
            cell.imageRate2.image = #imageLiteral(resourceName: "ic_fill_star")
            cell.imageRate3.image = #imageLiteral(resourceName: "ic_empty_star")
            cell.imageRate4.image = #imageLiteral(resourceName: "ic_empty_star")
            cell.imageRate5.image = #imageLiteral(resourceName: "ic_empty_star")
        } else if rate! >= 3 && rate! < 4 {
            cell.labelRatting.isHidden = false
            cell.labelRatting.text = "All Ready Rated"
            cell.buttonRatting.isUserInteractionEnabled = false
            cell.imageRate1.image = #imageLiteral(resourceName: "ic_fill_star")
            cell.imageRate2.image = #imageLiteral(resourceName: "ic_fill_star")
            cell.imageRate3.image = #imageLiteral(resourceName: "ic_fill_star")
            cell.imageRate4.image = #imageLiteral(resourceName: "ic_empty_star")
            cell.imageRate5.image = #imageLiteral(resourceName: "ic_empty_star")
        } else if rate! >= 4 && rate! < 5 {
            cell.labelRatting.isHidden = false
            cell.labelRatting.text = "All Ready Rated"
            cell.buttonRatting.isUserInteractionEnabled = false
            cell.imageRate1.image = #imageLiteral(resourceName: "ic_fill_star")
            cell.imageRate2.image = #imageLiteral(resourceName: "ic_fill_star")
            cell.imageRate3.image = #imageLiteral(resourceName: "ic_fill_star")
            cell.imageRate4.image = #imageLiteral(resourceName: "ic_fill_star")
            cell.imageRate5.image = #imageLiteral(resourceName: "ic_empty_star")
        } else if rate! >= 5 {
            cell.labelRatting.isHidden = false
            cell.labelRatting.text = "All Ready Rated"
            cell.buttonRatting.isUserInteractionEnabled = false
            cell.imageRate1.image = #imageLiteral(resourceName: "ic_fill_star")
            cell.imageRate2.image = #imageLiteral(resourceName: "ic_fill_star")
            cell.imageRate3.image = #imageLiteral(resourceName: "ic_fill_star")
            cell.imageRate4.image = #imageLiteral(resourceName: "ic_fill_star")
            cell.imageRate5.image = #imageLiteral(resourceName: "ic_fill_star")
        } else {
            if userStatus == "Finished" {
                cell.labelRatting.isHidden = false
                cell.labelRatting.text = "Rate Now"
                cell.buttonRatting.isUserInteractionEnabled = true
                cell.imageRate1.image = #imageLiteral(resourceName: "ic_empty_star")
                cell.imageRate2.image = #imageLiteral(resourceName: "ic_empty_star")
                cell.imageRate3.image = #imageLiteral(resourceName: "ic_empty_star")
                cell.imageRate4.image = #imageLiteral(resourceName: "ic_empty_star")
                cell.imageRate5.image = #imageLiteral(resourceName: "ic_empty_star")
            }
        }
        
        if userStatus == "Finished" {
            cell.labelStatus.textColor = UIColor.green
            cell.labelStatus.text = "COMPLETED"
        } else if userStatus == "Running" {
            cell.labelStatus.textColor = Define.APP_COLOR
            cell.labelStatus.text = "PENDING"
            cell.labelRatting.isHidden = true
            cell.buttonRatting.isUserInteractionEnabled = false
        } else if userStatus == "Terminated" {
            cell.labelStatus.textColor = UIColor.red
            cell.labelStatus.text = "CANCELLED"
            cell.labelRatting.isHidden = true
            cell.buttonRatting.isUserInteractionEnabled = false
        }
        
        cell.labelDate.text = dictData["AcceptDate"] as? String
        
        cell.buttonRatting.addTarget(self,
                                     action: #selector(self.buttonRateNow(_:)),
                                     for: .touchUpInside)
        cell.buttonRatting.tag = indexPath.row
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dictData = arrOffers[indexPath.row]
        let userStatus = dictData["UserStatus"] as! String
        if userStatus == "Running" {
            MyModel().showTostMessage(alertMessage: "Still Running",
                                      alertController: self)
        } else {
            let offerID = dictData["RestaurantID"] as? String
            if offerID == nil {
                MyModel().showAlertMessage(alertTitle: "Alert",
                                           alertMessage: "Offer Not Available.",
                                           alertController: self)
            } else {
                let completeVC = self.storyboard?.instantiateViewController(withIdentifier: "WalkCompleteVC") as! WalkCompleteVC
                completeVC.dictOffer = arrOffers[indexPath.row]
                completeVC.isFromStepCount = false
                completeVC.walkerOfferID = dictData["WalkerOfferID"] as! String
                self.navigationController?.pushViewController(completeVC, animated: true)
            }
        }
    }
}

//MARK: - API
extension OfferAvailVC {
    func getHisrotyAPI() {
        self.showLoadingView()
        let parameter: [String: Any] = ["WalkerID": Define.USERDEFAULT.value(forKey: "WalkerID")!]
        let strURL = Define.API_BASE_URL + Define.API_USER_HISTORY
        print("Parameter: ", parameter, "\nURL: ", strURL)
        SwiftAPI().postGetActiveOfferMethod(stringURL: strURL,
                                            userID: Define.USERDEFAULT.value(forKey: "UserID") as! String,
                                            parameter: parameter,
                                            header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result, error) in
            if let error = error {
                self.hideLoadingView()
                print("Error: ", error)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.getHisrotyAPI()
            } else {
                self.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    self.arrOffers = result!["data"] as! [[String: Any]]
                    self.tableOfferAvail.reloadData()
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
}


//MARK: - Ratting Delegate Method
extension OfferAvailVC: RattingDelegate {
    func ratedSuccessfull() {
        dismiss(animated: true, completion: nil)
        self.getHisrotyAPI()
    }
}
