
import UIKit
import SDWebImage
import GooglePlaces

class UserProfileVC: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var buttonEditProfile: UIButton!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var buttonEditProfileImage: UIButton!
    @IBOutlet weak var textFirstName: FloatLabelTextField!
    @IBOutlet weak var textLastName: FloatLabelTextField!
    @IBOutlet weak var textEmail: FloatLabelTextField!
    @IBOutlet weak var textMobileNo: FloatLabelTextField!
    @IBOutlet weak var textCity: FloatLabelTextField!
    @IBOutlet weak var textGender: FloatLabelTextField!
    @IBOutlet weak var textDateOfBirth: FloatLabelTextField!
    @IBOutlet weak var buttonLoaclity: UIButton!
    @IBOutlet weak var buttonGender: UIButton!
    @IBOutlet weak var buttonDateOfBirth: UIButton!
    @IBOutlet weak var buttonSaveProfile: UIButton!
    @IBOutlet weak var imageEmailDiasable: UIImageView!
    @IBOutlet weak var imageMobileNoDisable: UIImageView!
    @IBOutlet weak var imageGender: UIImageView!
    @IBOutlet weak var imageDateOfBirth: UIImageView!
    
    @IBOutlet weak var constraintSaveProfileButtunHeight: NSLayoutConstraint!
    
    
    private var dropDown: MyDropDown? = nil
    private let arrGender = ["Male",
                             "Female"]
    
    var datePicker: DatePickerDialog? = nil
    var dateOfBirth: Date? = nil
    
    var imagePicker: UIImagePickerController? = nil
    
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let strImageURL = Define.USERDEFAULT.value(forKey: "Image") as! String
        imageProfile.sd_setImage(with: URL(string: strImageURL))
        self.setUserInteraction(isSet: false)
        self.setUserDetail()
        setDatePickerDialog()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        self.viewNavigation.backgroundColor = Define.APP_COLOR
        self.viewProfile.backgroundColor = Define.APP_COLOR
        
        buttonEditProfileImage.layer.cornerRadius = buttonEditProfileImage.frame.height / 2
        buttonEditProfileImage.clipsToBounds = true
        buttonSaveProfile.backgroundColor = Define.APP_COLOR
        buttonSaveProfile.clipsToBounds = true
        Design().setProfileImage(image: imageProfile)
    }
    
    func setDatePickerDialog() {
        datePicker = DatePickerDialog(textColor: Define.APP_COLOR,
                                      buttonColor: Define.BUTTON_COLOR,
                                      font: UIFont(name: "MyriadHebrew-Regular", size: 15.0)!,
                                      showCancelButton: true)
    }
    
    func setUserDetail() {
        textFirstName.text = Define.USERDEFAULT.value(forKey: "FirstName") as? String
        textLastName.text = Define.USERDEFAULT.value(forKey: "LastName") as? String
        textEmail.text = Define.USERDEFAULT.value(forKey: "EmailID") as? String
        textMobileNo.text = Define.USERDEFAULT.value(forKey: "MobileNo") as? String
        textGender.text = Define.USERDEFAULT.value(forKey: "Gender") as? String
        textCity.text = Define.USERDEFAULT.value(forKey: "City") as? String
        
        let strBirthDate = Define.USERDEFAULT.value(forKey: "BirthDate") as! String
        if !strBirthDate.isEmpty {
            self.textDateOfBirth.text = MyModel().dateFormatterStringToString(date: strBirthDate)
            dateOfBirth = MyModel().dateFormatterStringToDate(date: strBirthDate)
        }
        
        
    }
    
    func setUserInteraction(isSet: Bool) {
        buttonEditProfile.isHidden = isSet
        buttonEditProfile.isUserInteractionEnabled = !isSet
        buttonEditProfileImage.isHidden = !isSet
        buttonEditProfileImage.isUserInteractionEnabled = isSet
        textFirstName.isUserInteractionEnabled = isSet
        textLastName.isUserInteractionEnabled = isSet
        textEmail.isUserInteractionEnabled = false
        textMobileNo.isUserInteractionEnabled = false
        
        buttonLoaclity.isUserInteractionEnabled = isSet
        buttonGender.isUserInteractionEnabled = isSet
        buttonDateOfBirth.isUserInteractionEnabled = isSet
        buttonSaveProfile.isUserInteractionEnabled = isSet
        
        imageEmailDiasable.isHidden = !isSet
        imageMobileNoDisable.isHidden = !isSet
        imageGender.isHidden = !isSet
        imageDateOfBirth.isHidden = !isSet
        
        if isSet {
            labelTitle.text = "Edit Profile"
            constraintSaveProfileButtunHeight.constant = 50
        } else {
            labelTitle.text = "My Profile"
            constraintSaveProfileButtunHeight.constant = 0
        }
    }
    
    
    //MARK: - Button Method
    @IBAction func buttonBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonEditProfil(_ sender: Any) {
        self.setUserInteraction(isSet: true)
    }
    
    @IBAction func buttonEditProfileImage(_ sender: Any) {
        imagePicker = UIImagePickerController()
        imagePicker!.delegate = self
        chooseOption()
    }
    
    @IBAction func buttonLocality(_ sender: Any) {
        let autoCompleteViewController = GMSAutocompleteViewController()
        autoCompleteViewController.delegate = self
        self.present(autoCompleteViewController, animated: true, completion: nil)
    }
    
    @IBAction func buttonGender(_ sender: Any) {
        if dropDown == nil {
            dropDown = MyDropDown()
            dropDown!.delegate = self
            dropDown!.showMyDropDown(sendButton: buttonGender,
                                     height: 80,
                                     arrayList: arrGender,
                                     imageList: nil,
                                     direction: "Down")
        } else {
            dropDown!.hideMyDropDown(sendButton: buttonGender)
            dropDown = nil
        }
    }
    
    @IBAction func buttonDateOfBirth(_ sender: Any) {
        datePicker!.show("Select Date Of Date",
                         doneButtonTitle: "Done",
                         cancelButtonTitle: "Cencel",
                         defaultDate: dateOfBirth ?? Date(),
                         minimumDate: nil,
                         maximumDate: Date(),
                         datePickerMode: .date)
        { (date) in
            if let selectedDate = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                self.textDateOfBirth.text = formatter.string(from: selectedDate)
                self.dateOfBirth = selectedDate
            }
        }
    }
    
    @IBAction func buttonSaveProfile(_ sender: Any) {
        if textFirstName.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Enter First Name", alertController: self)
        } else if textFirstName.text!.count < 3 {
            MyModel().showTostMessage(alertMessage: "Enter Proper First Name", alertController: self)
        } else if textLastName.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Enter Last Name", alertController: self)
        } else if textLastName.text!.count < 3 {
            MyModel().showTostMessage(alertMessage: "Enter Proper Last Name", alertController: self)
        } else if textDateOfBirth.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Select Date Of Birth", alertController: self)
        } else if !MyModel().isConnectedToInternet() {
            MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET, alertController: self)
        } else {
            self.updateUserprofileAPI()
        }
    }
}

//MARK: - DropDown Delegate Method
extension UserProfileVC: MyDropDownDelegate {
    func recievedSelectedValue(name: String) {
        self.textGender.text = name
        dropDown!.hideMyDropDown(sendButton: buttonGender)
        dropDown = nil
    }
}

extension UserProfileVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    // MARK: - Set Profile Method.
    func chooseOption() {
        let alertController = UIAlertController (title: nil, message: "Select Option", preferredStyle: .actionSheet)
        let cameraOption = UIAlertAction (title: "Camera", style: .default){
            (action: UIAlertAction) in
            self.fromCamera()
        }
        let photosOption = UIAlertAction (title: "Photos", style: .default){
            (action: UIAlertAction) in
            self.fromPhotos()
        }
        let cancelOption = UIAlertAction (title: "Cancel", style: .cancel)
        
        alertController.addAction(cameraOption)
        alertController.addAction(photosOption)
        alertController.addAction(cancelOption)
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            let popPresenter = alertController.popoverPresentationController
            popPresenter?.sourceView = imageProfile
            popPresenter?.sourceRect = imageProfile.bounds
            self.present(alertController, animated: true, completion: nil)
        }else{
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func fromCamera() {
        imagePicker!.allowsEditing = true
        imagePicker!.sourceType = UIImagePickerControllerSourceType.camera
        self.present(imagePicker!, animated: true, completion: nil)
    }
    
    func fromPhotos() {
        imagePicker!.allowsEditing = true
        imagePicker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(imagePicker!, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage

        imageProfile.contentMode = .scaleAspectFill
        imageProfile.image = chosenImage
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

//MARK: - Google Delegate Method
extension UserProfileVC: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        print("Name : \(place.name)")
        print("Latitude : \(place.coordinate.latitude)")
        print("Longitude : \(place.coordinate.longitude)")
        
        textCity.text = place.name
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error : \(error.localizedDescription)")
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
}

//MARK: - API
extension UserProfileVC {
    func updateUserprofileAPI() {
        Define.APPDELEGATE.showLoadingView()
        let parameter: [String: Any] = ["WalkerID": Define.USERDEFAULT.value(forKey: "WalkerID")!,
                                        "FirstName": textFirstName.text!,
                                        "LastName": textLastName.text!,
                                        "Gender": textGender.text!,
                                        "Birthdate": MyModel().dateFormatterToString(date: dateOfBirth!),
                                        "City": textCity.text!]
        let strURL = Define.API_BASE_URL + Define.API_UPDATE_WALKER
        print("Parameter: ", parameter, "\nURL: ", strURL)
        
        var imageData: Data?
        
        if imageProfile.image != nil {
            imageData = UIImageJPEGRepresentation(imageProfile.image!, 0.8)!
        }
        
        SwiftAPI().postImageUplodOfferAndProfile(stringURL: strURL,
                                                 parameters: parameter,
                                                 userID: Define.USERDEFAULT.value(forKey: "UserID")! as! String,
                                                 header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String,
                                                 imageName: "Profile_Image",
                                                 imageData: imageData)
        { (result, error) in
            if let error = error {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.updateUserprofileAPI()
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    self.setUserInteraction(isSet: false)
                    MyModel().showTostMessage(alertMessage: "Profile Updated Successfully",
                                              alertController: self)
                    let data = result!["data"] as! [String: Any]
                    Define.USERDEFAULT.set(data["FirstName"]!, forKey: "FirstName")
                    Define.USERDEFAULT.set(data["LastName"]!, forKey: "LastName")
                    Define.USERDEFAULT.set(data["EmailID"]!, forKey: "EmailID")
                    Define.USERDEFAULT.set(data["MobileNo"]!, forKey: "MobileNo")
                    Define.USERDEFAULT.set(data["Birthdate"]!, forKey: "BirthDate")
                    Define.USERDEFAULT.set(data["Gender"]!, forKey: "Gender")
                    Define.USERDEFAULT.set(data["City"]!, forKey: "City")
                    Define.USERDEFAULT.set(data["ProfileImgPath"]!, forKey: "Image")
                    Define.USERDEFAULT.set(data["Age"]!, forKey: "Age")
                    
                    //Health
                    Define.USERDEFAULT.set(data["Weight"]!, forKey: "Weight")
                    Define.USERDEFAULT.set(data["Height"]!, forKey: "Height")
                    Define.USERDEFAULT.set(data["Diabetes"]!, forKey: "Diabetes")
                    Define.USERDEFAULT.set(data["BloodPressure"]!, forKey: "BloodPressure")
                    Define.USERDEFAULT.set(data["Allergy"]!, forKey: "Allergy")
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
}
