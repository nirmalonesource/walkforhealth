import UIKit

class OfferListTVC: UITableViewCell {

    @IBOutlet weak var viewOffer: UIView!
    @IBOutlet weak var labelOfferName: UILabel!
    @IBOutlet weak var labelDiscount: UILabel!
    @IBOutlet weak var labelOfferDetail: UILabel!
    @IBOutlet weak var buttonViewOffer: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Design().viewWithShadow(view: viewOffer)
        Design().setButton(button: buttonViewOffer)
        Design().setLabel(label: labelOfferName)
        Design().setLabel(label: labelDiscount)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
