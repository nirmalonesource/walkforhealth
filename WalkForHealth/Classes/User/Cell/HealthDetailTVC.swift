import UIKit

class HealthDetailTVC: UITableViewCell {

    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var viewStepCount: UIView!
    @IBOutlet weak var viewDistance: UIView!
    @IBOutlet weak var labelCaloriesBorn: UILabel!
    @IBOutlet weak var labelRequireCalories: UILabel!
    @IBOutlet weak var labelStepCount: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var viewCalories: UIView!
    @IBOutlet weak var viewCaloriesBackground: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Design().viewWithShadow(view: viewCell)
        labelDate.textColor = Define.APP_COLOR
        self.setView(view: viewStepCount)
        self.setView(view: viewDistance)
        viewCalories.backgroundColor = Define.APP_COLOR
        viewCaloriesBackground.backgroundColor = Define.APP_COLOR
        viewCalories.layer.cornerRadius = 8
        viewCalories.clipsToBounds = true
    }
    
    func setView(view: UIView) {
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor(red: 52/255.0, green: 152/255.0, blue: 219/255.0, alpha: 0.6).cgColor
        view.layer.cornerRadius = view.frame.height / 2
        view.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }

}
