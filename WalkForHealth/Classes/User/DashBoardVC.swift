import UIKit
import GoogleMaps
import GooglePlaces
import HealthKit
import CoreMotion
import SDWebImage
import AVFoundation

class DashBoardVC: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var imageHealth: UIImageView!
    @IBOutlet weak var imageExplore: UIImageView!
    @IBOutlet weak var imageOffers: UIImageView!
    @IBOutlet weak var labelHealth: UILabel!
    @IBOutlet weak var labelExplore: UILabel!
    @IBOutlet weak var labelOffers: UILabel!
    @IBOutlet weak var buttonHealth: UIButton!
    @IBOutlet weak var buttonExplore: UIButton!
    @IBOutlet weak var buttonOffers: UIButton!
    
    @IBOutlet weak var viewMap: GMSMapView!
    @IBOutlet weak var viewHealth: UIView!
    @IBOutlet weak var viewOffers: UIView!
    //Explore
    @IBOutlet weak var viewSearchMain: UIView!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var textSearch: UITextField!
    @IBOutlet weak var buttonCurrentLocation: UIButton!
    @IBOutlet weak var buttonOptionFilter: UIButton!
    @IBOutlet weak var buttonCloseFilter: UIButton!
    @IBOutlet weak var constraintViewSearchMainHeight: NSLayoutConstraint!
    @IBOutlet weak var buttonHandelFilter: UIButton!
    
    //Health
    @IBOutlet weak var viewStepCount: UIView!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var imageStep: UIImageView!
    @IBOutlet weak var buttonViewDetails: UIButton!
    @IBOutlet weak var viewHealthTips: UIView!
    @IBOutlet weak var viewHealthNavigation: UIView!
    @IBOutlet weak var labelStepCount: UILabel!
    @IBOutlet weak var labelBurnCalories: UILabel!
    @IBOutlet weak var labelRequiredCalories: UILabel!
    @IBOutlet weak var scrollViewHealthTips: UIScrollView!
    @IBOutlet weak var pageControlHealthTips: UIPageControl!
    
    //Offers
    @IBOutlet weak var viewOffersNavigation: UIView!
    @IBOutlet weak var tableRestaurant: UITableView!
    @IBOutlet weak var viewNoData: UIView!
    @IBOutlet weak var labelNoData: UILabel!
    
    // Ad
    @IBOutlet weak var viewAdvertiesment: UIView!
    @IBOutlet weak var imageBanner: UIImageView!
    
    @IBOutlet weak var constraintImageBannerHeight: NSLayoutConstraint!
    
    //Search Filter
    @IBOutlet weak var viewSearchFilter: UIView!
    @IBOutlet weak var buttonFromKMS: UIButton!
    @IBOutlet weak var buttonToKMS: UIButton!
    
    private var isHealthView = false
    private var isExploreView = true
    private var isOffersView = false
    private var isLoaded = false
    private var isRestaurantListLoad = Bool()
    
    //Location
    private var locationMaeneger: CLLocationManager?
    private var latitude = Double()
    private var longitude = Double()
    private var bounds = GMSCoordinateBounds()
    private var infoWindow = MarkerView()
    fileprivate var locationMarker : GMSMarker? = GMSMarker()
    private var currentLocation = GMSMarker()
    
    private var arrRestaurantList = [[String: Any]]()
    private var arrHealthTips = [[String: Any]]()
    
    private var humanWeight = Float()
    private var humanHeight = Float()
    private var humanMeasure = Float()
    
    private var startLocation: CLLocation? = nil
    private var endLocation: CLLocation? = nil
    
    private var dropDownOBJ: MyDropDown? = nil
    private let arrFilterKMS = ["1kms", "2kms", "3kms", "4kms", "5kms", "6kms", "7kms", "8kms", "9kms","10kms"]
    private let dictFilterKMS: [String: Int] = ["1kms": 1,
                                                "2kms": 2,
                                                "3kms": 3,
                                                "4kms": 4,
                                                "5kms": 5,
                                                "6kms": 6,
                                                "7kms": 7,
                                                "8kms": 8,
                                                "9kms": 9,
                                                "10kms": 10]
    private var isFromKMS = Bool()
    private var isToKMS = Bool()
    private var fromKiloMeter = 10
    private var toKiloMeter = 0
    private var isFromSearchCity = Bool()
    private var cityLatitude = Double()
    private var cityLongitude = Double()
    
    private var slider: [ViewSlider] = []
    
    var buttonFilterOptionview: UIButton? = nil
    
    
    var isRefresh = Bool()
    lazy  var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = Define.APP_COLOR
        return refreshControl
    }()
    
    private var sliderTips: [ViewSlider] = []
    var player: AVPlayer!
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()
        tableRestaurant.rowHeight = UITableViewAutomaticDimension
        tableRestaurant.addSubview(refreshControl)
        self.infoWindow = loadNib()
        currentLocation.icon = #imageLiteral(resourceName: "ic_streetviewicon")
        checkLocationSerVices()
        self.getAdvertisementListAPI()
        self.getHealthTips()
        viewNoData.isHidden = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        Design().viewSerchWithShadow(view: viewSearch)
        Design().viewWithShadow(view: viewStepCount)
        Design().viewWithShadow(view: viewHealthTips)
        buttonCurrentLocation.layer.cornerRadius = buttonCurrentLocation.frame.height / 2
        buttonCurrentLocation.clipsToBounds = true
        buttonCurrentLocation.layer.borderWidth = 0.5
        buttonCurrentLocation.layer.borderColor = UIColor.lightGray.cgColor
        
        viewHealthNavigation.backgroundColor = Define.APP_COLOR
        viewOffersNavigation.backgroundColor = Define.APP_COLOR
        imageStep.layer.cornerRadius = imageStep.frame.height / 2
        imageStep.clipsToBounds = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !isLoaded {
            self.viewHealth.center.x -= self.view.bounds.width
            self.viewOffers.center.x += self.view.bounds.width
            self.viewSearchFilter.center.x += self.view.bounds.width
            viewHealth.isHidden = false
            viewOffers.isHidden = false
            viewSearchFilter.isHidden = false
            isLoaded = true
        }
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        textSearch.text = nil
        buttonCloseFilter.isHidden = true;
        isRefresh = true
        refreshControl.beginRefreshing()
        //self.getRestaurantList(lat: latitude, long: longitude, keyword: textSearch.text!, km: 5, toKM: 0)
        self.getRestaurantSearchData(lat: "",
                                     long: "",
                                     km: 5,
                                     toKM: 0,
                                     strRestuarant: textSearch.text!,
                                     strCity: "")
    }
    
    @objc func handelSearch(_ textField: UITextField) {
        if textField.text!.count > 0{
            buttonCloseFilter.isHidden = false
            //self.getRestaurantList(lat: latitude, long: longitude, keyword: textField.text!, km: 5, toKM: 0)
            self.getRestaurantSearchData(lat: "",
                                         long: "",
                                         km: 0,
                                         toKM: 0,
                                         strRestuarant: textField.text!,
                                         strCity: "")
        }
    }
    
    func setData() {
        humanWeight = Float(Define.USERDEFAULT.value(forKey: "Weight") as! String)!
        humanHeight = Float(Define.USERDEFAULT.value(forKey: "Height") as! String)!
        let gender = Define.USERDEFAULT.value(forKey: "Gender") as! String
        if gender == "Male" {
            humanMeasure = 0.415
        } else if gender == "Female" {
            humanMeasure = 0.413
        } else {
            humanMeasure = 0.414
        }
    }
    
    func checkLocationSerVices() {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                    determineMyCurrentLocation()
                    createSettingsAlertController(title: "ALert",
                                                  message: "To enable access jump to setting.")
                case .authorizedAlways, .authorizedWhenInUse:
                    determineMyCurrentLocation()
            }
        } else {
            createSettingsAlertController(title: "ALert", message: "To enable location jump to setting.")
        }
    }
    
    //MARK: - Map
    func setMap() {
        viewMap.isMyLocationEnabled = false
        let camera = GMSCameraPosition.camera(withLatitude: latitude,
                                              longitude: longitude,
                                              zoom: 16)
        viewMap.animate(to: camera)
        viewMap.delegate = self
        viewMap.mapType = .normal
    }
    
    //Set Marker
    func setMarker() {
        for dictData in arrRestaurantList {
            let dataType = dictData["DataType"] as! String
            if dataType == "Restaurant" {
                let lat = Double(dictData["Latitude"] as! String)
                let long = Double(dictData["Longitude"] as! String)
                let position = CLLocationCoordinate2DMake(lat!, long!)
                let marker = GMSMarker(position: position)
                marker.icon = #imageLiteral(resourceName: "ic_set_marker")
                marker.map = viewMap
                marker.userData = dictData
                //bounds = bounds.includingCoordinate(marker.position)
            } else {
               print("Advertisement")
            }
        }
        //let update = GMSCameraUpdate.fit(bounds, withPadding: 60)
        //viewMap.animate(with: update)
        
    }
    
    //Set MarkerView
    func setmarkerView() {
        
    }
    
    func checkAuthentication() {
        let stepQuantityType = HKQuantityType.quantityType(forIdentifier: .stepCount)!
        let distanceQuantityType = HKQuantityType.quantityType(forIdentifier: .distanceWalkingRunning)
        
        HKHealthStore().requestAuthorization(toShare: nil,
                                             read: [stepQuantityType, distanceQuantityType!])
        { (success, error) in
            if error != nil {
                print("Error: ", error!)
            } else {
                print("Success: ", success)
                self.getTodayDistance(completion: { (distance) in
                    let distanceMile = Measurement(value: distance, unit: UnitLength.miles)
                    let distanceKM = distanceMile.converted(to: UnitLength.kilometers).value
                    print("KM : ", distanceKM)
                    let strDistance = String(format: "%.2f", distanceKM as Double)
                    self.setLabelDistance(strDistance: strDistance)
                })
                self.getTodayStepCount(completion: { (step) in
                    let strStep = String(format: "%.0f", step)
                    self.setLabelStepCount(strStep: strStep)
                })
            }
        }
    }
    
    func setLabelStepCount(strStep: String) {
        DispatchQueue.main.async {
            self.labelStepCount.text = strStep
            let strhumanWeight = Define.USERDEFAULT.value(forKey: "Weight") as! String
            
            if !strhumanWeight.isEmpty {
                self.setData()
                let calories = self.getPerStepCalories(weight: Double(self.humanWeight), stepSize: Double(self.humanMeasure))
                let totalBurnCalories = Double(strStep)! * calories
                print("Total Calories: ", totalBurnCalories)
                let strTotalBurnCalories = String(format: "%.0f", totalBurnCalories)
                self.labelBurnCalories.text = strTotalBurnCalories
                
                // Required Calories
                let age = Define.USERDEFAULT.value(forKey: "Age") as! Int
                self.labelRequiredCalories.text = MyModel().getRequiredCalories(weight: Double(self.humanWeight),
                                                                           height: Double(self.humanHeight),
                                                                           age: age)
            }
        }
    }
    
    func setLabelDistance(strDistance: String) {
        DispatchQueue.main.async {
            self.labelDistance.text = strDistance
        }
    }
    
    func getTodayStepCount(completion: @escaping (Double) -> Void) {
        let healthData = HKHealthStore()
        let stepQuantityType = HKQuantityType.quantityType(forIdentifier: .stepCount)!
        let now = Date()
        let startOfDay =  Calendar.current.startOfDay(for: now) //Calendar.current.date(byAdding: .day, value: -1, to: Date())
        let predicate = HKQuery.predicateForSamples(withStart: startOfDay, end: now, options: .strictStartDate)
        let query = HKStatisticsQuery(quantityType: stepQuantityType,
                                      quantitySamplePredicate: predicate,
                                      options: .cumulativeSum)
        { (_, result, error) in
            if error != nil {
                print("Error: ", error!)
            }
            
            guard let result = result, let sum = result.sumQuantity() else {
                completion(0.0)
                return
            }
            let step = sum.doubleValue(for: HKUnit.count())
            print("Step: ", step)
            completion(sum.doubleValue(for: HKUnit.count()))
        }
        healthData.execute(query)
    }
    
    func getTodayDistance(completion: @escaping (Double) -> Void) {
        let healthData = HKHealthStore()
        let distanceQuantityType = HKQuantityType.quantityType(forIdentifier: .distanceWalkingRunning)
        let now = Date()
        let startOfDay = Calendar.current.startOfDay(for: now)
        let predicate = HKQuery.predicateForSamples(withStart: startOfDay, end: now, options: .strictStartDate)
        let query = HKStatisticsQuery(quantityType: distanceQuantityType!,
                                      quantitySamplePredicate: predicate,
                                      options: .cumulativeSum)
        { (_, result, error) in
            if error != nil {
                print("Error: ", error!)
            }
            
            guard let result = result, let sum = result.sumQuantity() else {
                completion(0.0)
                return
            }
            print("Result Distance: ", result)
            let distance = sum.doubleValue(for: HKUnit.mile())
            print("Distance: ", distance)
            completion(sum.doubleValue(for: HKUnit.mile()))
        }
        healthData.execute(query)
    }
    
    func loadNib() -> MarkerView {
        let infoView = MarkerView.instanceFromNib() as! MarkerView
        return infoView
    }
    

    //MARK: - Button Method
    @IBAction func buttonHealth(_ sender: Any) {
        if isOffersView {
            buttonExplore.isEnabled = false
            buttonExplore.isEnabled = false
            UIView.animate(withDuration: 0.3, delay: 0.1, options: [], animations: {
                self.viewOffers.center.x += self.view.bounds.width
                self.viewHealth.center.x += self.view.bounds.width
            }) { _ in
                self.buttonExplore.isEnabled = true
                self.buttonExplore.isEnabled = true
                
                self.viewSearchMain.center.x = self.viewOffers.center.x
                self.buttonCurrentLocation.center.x += self.view.bounds.width * 2
            }
        }else{
            if !isHealthView {
                buttonExplore.isEnabled = false
                buttonExplore.isEnabled = false
                UIView.animate(withDuration: 0.3, delay: 0.1, options: [], animations: {
                    self.viewSearchMain.center.x += self.view.bounds.width
                    self.buttonCurrentLocation.center.x += self.view.bounds.width
                    self.viewHealth.center.x += self.view.bounds.width
                }) { _ in
                    self.buttonExplore.isEnabled = true
                    self.buttonExplore.isEnabled = true
                }
            }
        }
        
        checkAuthentication()
        
        isHealthView = true
        isExploreView = false
        isOffersView = false
        
        imageHealth.image = #imageLiteral(resourceName: "ic_dashboard_health_select")
        imageExplore.image = #imageLiteral(resourceName: "ic_dashboard_explore_unselect")
        imageOffers.image = #imageLiteral(resourceName: "ic_dashboard_offers_unselect")
        
        labelHealth.textColor = Define.APP_COLOR
        labelExplore.textColor = Define.LABEL_DARK_COLOR
        labelOffers.textColor = Define.LABEL_DARK_COLOR
    }
    
    func determineMyCurrentLocation() {
        locationMaeneger = CLLocationManager()
        locationMaeneger!.delegate = self
        locationMaeneger!.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationMaeneger!.requestAlwaysAuthorization()
        locationMaeneger!.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            locationMaeneger!.startUpdatingLocation()
        }
    }
    
    @IBAction func buttonExplore(_ sender: Any) {
        exploreAnimation()
    }
    @IBAction func buttonOffers(_ sender: Any) {
        if isHealthView {
            buttonExplore.isEnabled = false
            buttonHealth.isEnabled = false
            UIView.animate(withDuration: 0.3, delay: 0.1, options: [], animations: {
                self.viewHealth.center.x -= self.view.bounds.width
                self.viewOffers.center.x -= self.view.bounds.width
            }) { _ in
                self.buttonExplore.isEnabled = true
                self.buttonHealth.isEnabled = true
                self.viewSearchMain.center.x = self.viewHealth.center.x
                self.buttonCurrentLocation.center.x -= self.view.bounds.width * 2
            }
        } else {
            if !isOffersView {
                buttonExplore.isEnabled = false
                buttonHealth.isEnabled = false
                UIView.animate(withDuration: 0.3, delay: 0.1, options: [], animations: {
                    self.viewSearchMain.center.x -= self.view.bounds.width
                    self.buttonCurrentLocation.center.x -= self.view.bounds.width
                    self.viewOffers.center.x -= self.view.bounds.width
                }) { _ in
                    self.buttonExplore.isEnabled = true
                    self.buttonHealth.isEnabled = true
                }
            }
        }
        isHealthView = false
        isExploreView = false
        isOffersView = true
        
        imageHealth.image = #imageLiteral(resourceName: "ic_dashboard_health_unselect")
        imageExplore.image = #imageLiteral(resourceName: "ic_dashboard_explore_unselect")
        imageOffers.image = #imageLiteral(resourceName: "ic_dashboard_offers_select")
        
        labelHealth.textColor = Define.LABEL_DARK_COLOR
        labelExplore.textColor = Define.LABEL_DARK_COLOR
        labelOffers.textColor = Define.APP_COLOR
    }
    
    @IBAction func buttonMenu(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    
    @IBAction func buttonCurrentLocation(_ sender: Any) {
        //viewMap.isMyLocationEnabled = true
        let camera = GMSCameraPosition.camera(withLatitude: latitude,
                                              longitude: longitude,
                                              zoom: 16)
        viewMap.animate(to: camera)
    }
    @IBAction func buttonHealthBack(_ sender: Any) {
        exploreAnimation()
    }
    @IBAction func buttonOffersBack(_ sender: Any) {
        exploreAnimation()
    }
    
    @objc func buttonCheckOffer(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tableRestaurant)
        let indexPath = tableRestaurant.indexPathForRow(at: buttonPosition) as IndexPath?
        
        let offerVC = self.storyboard?.instantiateViewController(withIdentifier: "OfferListVC") as! OfferListVC
        offerVC.dictOffer = arrRestaurantList[indexPath!.row]
        self.navigationController?.pushViewController(offerVC, animated: true)
    }
    
    func exploreAnimation() {
        if isHealthView {
            buttonHealth.isEnabled = false
            buttonOffers.isEnabled = false
            
            UIView.animate(withDuration: 0.3, delay: 0.1, options: [], animations: {
                self.viewHealth.center.x -= self.view.bounds.width
                self.viewSearchMain.center.x -= self.view.bounds.width
                self.buttonCurrentLocation.center.x -= self.view.bounds.width
            }) { _ in
                self.buttonHealth.isEnabled = true
                self.buttonOffers.isEnabled = true
            }
        } else if isOffersView {
            buttonHealth.isEnabled = false
            buttonOffers.isEnabled = false
            UIView.animate(withDuration: 0.3, delay: 0.1, options: [], animations: {
                self.viewOffers.center.x += self.view.bounds.width
                self.viewSearchMain.center.x += self.view.bounds.width
                self.buttonCurrentLocation.center.x += self.view.bounds.width
            }) { _ in
                self.buttonHealth.isEnabled = true
                self.buttonOffers.isEnabled = true
            }
        }
        
        isHealthView = false
        isExploreView = true
        isOffersView = false
        
        imageHealth.image = #imageLiteral(resourceName: "ic_dashboard_health_unselect")
        imageExplore.image = #imageLiteral(resourceName: "ic_dashboard_explore_select")
        imageOffers.image = #imageLiteral(resourceName: "ic_dashboard_offers_unselect")
        
        labelHealth.textColor = Define.LABEL_DARK_COLOR
        labelExplore.textColor = Define.APP_COLOR
        labelOffers.textColor = Define.LABEL_DARK_COLOR
    }
    @IBAction func buttonFindOffers(_ sender: Any) {
        //let rateVC = self.storyboard?.instantiateViewController(withIdentifier: "RattingNC")
        //self.present(rateVC!, animated: true, completion: nil)
    }
    
    @IBAction func buttonHealthDetail(_ sender: Any) {
        let healthVC = self.storyboard?.instantiateViewController(withIdentifier: "HealthDetailNC")
        self.present(healthVC!, animated: true, completion: nil)
    }
    
    @IBAction func buttonCloseAd(_ sender: Any) {
        if player != nil {
            player!.pause()
        }
        self.viewAdvertiesment.removeFromSuperview()
    }
    
    @IBAction func buttonShowText(_ sender: Any) {
        let infoView = ViewTextInfo.instanceFromNib() as! ViewTextInfo
        self.view.addSubview(infoView)
        infoView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            infoView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            infoView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
            infoView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            infoView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor)
            ])
    }
    
    @IBAction func buttonOptionFilter(_ sender: Any) {
        let filterView = ViewFilter.instanceFromNib() as! ViewFilter
        filterView.delegate = self
        self.view.addSubview(filterView)
        filterView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            filterView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            filterView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
            filterView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            filterView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor)
            ])
    }
    
    @IBAction func buttonHandelFilter(_ sender: Any) {
        print("Filter Click")
        let autoCompleteViewController = GMSAutocompleteViewController()
        let filter = GMSAutocompleteFilter()
        filter.country = "IN"
        filter.type = .city
        autoCompleteViewController.autocompleteFilter = filter
        autoCompleteViewController.delegate = self
        self.present(autoCompleteViewController, animated: true, completion: nil)
    }
    
    @IBAction func buttonCloseFilter(_ sender: Any) {
        textSearch.text = nil
        buttonCloseFilter.isHidden = true
        //self.getRestaurantList(lat: latitude, long: longitude, keyword: "", km: 5, toKM: 0)
        self.getRestaurantSearchData(lat: "",
                                     long: "",
                                     km: 5,
                                     toKM: 0,
                                     strRestuarant: "",
                                     strCity: "")
    }
    
    @IBAction func buttonSearchFilterMenu(_ sender: Any) {
        fromKiloMeter = 0
        toKiloMeter = 0
        
        self.buttonFromKMS.setTitle("Select", for: .normal)
        self.buttonToKMS.setTitle("Select", for: .normal)
        
        UIView.animate(withDuration: 0.3,
                       delay: 0.1,
                       options: [], animations: {
                        self.viewSearchFilter.center.x -= self.view.bounds.width
        }) { _ in
            
        }
    }
    
    //TODO: SEARCH FILTER
    @IBAction func buttonhideSeachFilter(_ sender: Any) {
        fromKiloMeter = 0
        toKiloMeter = 0
        
        self.buttonFromKMS.setTitle("Select", for: .normal)
        self.buttonToKMS.setTitle("Select", for: .normal)
        
        if dropDownOBJ != nil {
            dropDownOBJ!.hideMyDropDown(sendButton: buttonFromKMS)
            dropDownOBJ = nil
        }
        
        UIView.animate(withDuration: 0.3,
                       delay: 0.1,
                       options: [], animations: {
                        self.viewSearchFilter.center.x += self.view.bounds.width
        }) { _ in
            
        }
    }
    
    @IBAction func buttonSearchFilter(_ sender: Any) {
        if fromKiloMeter == 0 && toKiloMeter == 0 {
            MyModel().showAlertMessage(alertTitle: "Alert",
                                       alertMessage: "Select Kilometers.",
                                       alertController: self)
        } else if fromKiloMeter > toKiloMeter {
            MyModel().showAlertMessage(alertTitle: "Alert",
                                       alertMessage: "To kilometer must be grater than from kilometer.",
                                       alertController: self)
        } else {
            //self.getRestaurantList(lat: latitude, long: longitude, keyword: "", km: fromKiloMeter, toKM: toKiloMeter)
            self.getRestaurantSearchData(lat: "",
                                         long: "",
                                         km: fromKiloMeter,
                                         toKM: toKiloMeter,
                                         strRestuarant: "",
                                         strCity: "")
            UIView.animate(withDuration: 0.3,
                           delay: 0.1,
                           options: [], animations: {
                            self.viewSearchFilter.center.x += self.view.bounds.width
            }) { _ in
                
            }
        }
    }
    
    @IBAction func buttonRefreshSearchFilter(_ sender: Any) {
        //self.getRestaurantList(lat: latitude, long: longitude, keyword: "", km: 5, toKM: 0)
        self.getRestaurantSearchData(lat: "",
                                     long: "",
                                     km: 5,
                                     toKM: 0,
                                     strRestuarant: "",
                                     strCity: "")
        UIView.animate(withDuration: 0.3,
                       delay: 0.1,
                       options: [], animations: {
                        self.viewSearchFilter.center.x += self.view.bounds.width
        }) { _ in
            
        }
    }
    
    @IBAction func buttonFromKMS(_ sender: Any) {
        isFromKMS = true
        isToKMS = false
        
        if dropDownOBJ == nil {
            dropDownOBJ = MyDropDown()
            dropDownOBJ!.delegate = self
            dropDownOBJ!.showMyDropDown(sendButton: buttonFromKMS,
                                        height: 240,
                                        arrayList: arrFilterKMS,
                                        imageList: nil,
                                        direction: "Down")
        } else {
            dropDownOBJ!.hideMyDropDown(sendButton: buttonFromKMS)
            dropDownOBJ = nil
        }
        
    }
    
    @IBAction func buttonToKMS(_ sender: Any) {
        isFromKMS = false
        isToKMS = true
        
        if dropDownOBJ == nil {
            dropDownOBJ = MyDropDown()
            dropDownOBJ!.delegate = self
            dropDownOBJ!.showMyDropDown(sendButton: buttonToKMS,
                                        height: 240,
                                        arrayList: arrFilterKMS,
                                        imageList: nil,
                                        direction: "Down")
        } else {
            dropDownOBJ!.hideMyDropDown(sendButton: buttonToKMS)
            dropDownOBJ = nil
        }
    }
}

//MARK: - Tableview Delegate Method
extension DashBoardVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRestaurantList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //TODO: SetData
        let dictData = arrRestaurantList[indexPath.row]
        
        let dataType = dictData["DataType"] as! String
        if dataType == "Restaurant" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurantTVC") as! RestaurantTVC
            let strName = dictData["RestaurantName"] as? String ?? "No Name"
            cell.labelRestaurantName.text = strName.uppercased()
            let strDistance = Double(dictData["Distance"] as? String ?? "0.0")
            cell.labelDistance.text = "\(String(format: "%.2f", strDistance!)) KM"
            //= "\(dictData["Distance"] as? NSNumber ?? 0) KM"
            cell.labelAddress.text = dictData["Locality"] as? String
            
            let arrOffers = dictData["Offers"] as! [[String: Any]]
            if arrOffers.count > 0 {
                let dictOffers = arrOffers[0]
                
                if let dictDiscount = dictOffers["Kilometer"] as? [[String: Any]] {
                    cell.labelDiscount.text = self.getMainOffer(arrOffers: dictDiscount)
                } else {
                    cell.labelDiscount.text = "0%"
                }
            } else {
                cell.labelDiscount.text = "0%"
            }
            
            if let strRate = dictData["Ratings"] as? String {
                let rate = Double(strRate)
                cell.labelRate.text = "(\(strRate))"
                if rate! >= 1.00 && rate! < 2.00 {
                    cell.imageStar1.image = #imageLiteral(resourceName: "ic_fill_star")
                    cell.imageStar2.image = #imageLiteral(resourceName: "ic_empty_star")
                    cell.imageStar3.image = #imageLiteral(resourceName: "ic_empty_star")
                    cell.imageStar4.image = #imageLiteral(resourceName: "ic_empty_star")
                    cell.imageStar5.image = #imageLiteral(resourceName: "ic_empty_star")
                } else if rate! >= 2.00 && rate! < 3.00 {
                    cell.imageStar1.image = #imageLiteral(resourceName: "ic_fill_star")
                    cell.imageStar2.image = #imageLiteral(resourceName: "ic_fill_star")
                    cell.imageStar3.image = #imageLiteral(resourceName: "ic_empty_star")
                    cell.imageStar4.image = #imageLiteral(resourceName: "ic_empty_star")
                    cell.imageStar5.image = #imageLiteral(resourceName: "ic_empty_star")
                } else if rate! >= 3.00 && rate! < 4.00 {
                    cell.imageStar1.image = #imageLiteral(resourceName: "ic_fill_star")
                    cell.imageStar2.image = #imageLiteral(resourceName: "ic_fill_star")
                    cell.imageStar3.image = #imageLiteral(resourceName: "ic_fill_star")
                    cell.imageStar4.image = #imageLiteral(resourceName: "ic_empty_star")
                    cell.imageStar5.image = #imageLiteral(resourceName: "ic_empty_star")
                } else if rate! >= 4.00 && rate! < 5 {
                    cell.imageStar1.image = #imageLiteral(resourceName: "ic_fill_star")
                    cell.imageStar2.image = #imageLiteral(resourceName: "ic_fill_star")
                    cell.imageStar3.image = #imageLiteral(resourceName: "ic_fill_star")
                    cell.imageStar4.image = #imageLiteral(resourceName: "ic_fill_star")
                    cell.imageStar5.image = #imageLiteral(resourceName: "ic_empty_star")
                } else if rate! >= 5 {
                    cell.imageStar1.image = #imageLiteral(resourceName: "ic_fill_star")
                    cell.imageStar2.image = #imageLiteral(resourceName: "ic_fill_star")
                    cell.imageStar3.image = #imageLiteral(resourceName: "ic_fill_star")
                    cell.imageStar4.image = #imageLiteral(resourceName: "ic_fill_star")
                    cell.imageStar5.image = #imageLiteral(resourceName: "ic_fill_star")
                } else {
                    cell.imageStar1.image = #imageLiteral(resourceName: "ic_empty_star")
                    cell.imageStar2.image = #imageLiteral(resourceName: "ic_empty_star")
                    cell.imageStar3.image = #imageLiteral(resourceName: "ic_empty_star")
                    cell.imageStar4.image = #imageLiteral(resourceName: "ic_empty_star")
                    cell.imageStar5.image = #imageLiteral(resourceName: "ic_empty_star")
                }
            } else {
                cell.imageStar1.image = #imageLiteral(resourceName: "ic_empty_star")
                cell.imageStar2.image = #imageLiteral(resourceName: "ic_empty_star")
                cell.imageStar3.image = #imageLiteral(resourceName: "ic_empty_star")
                cell.imageStar4.image = #imageLiteral(resourceName: "ic_empty_star")
                cell.imageStar5.image = #imageLiteral(resourceName: "ic_empty_star")
            }
            
            //TODO: SetButton
            cell.buttonCheckOffer.addTarget(self,
                                            action: #selector(self.buttonCheckOffer(_:)),
                                            for: .touchUpInside)
            cell.buttonCheckOffer.tag = indexPath.row
            return cell
        } else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AdvertisementCell") as! AdvertisementCell
            
            let imageHeight = dictData["Height"] as! Float
            let imageWidth = dictData["Width"] as! Float
            print("Image Siza: ", imageHeight, imageWidth)
            let deviceWidth = Float(self.view.frame.width)
            let adImageHeight = (deviceWidth * imageHeight) / imageWidth
            cell.constraintImageHeight.constant = CGFloat(adImageHeight)
            
            let strImageUrl = dictData["AdvImaPath"] as! String
            let activity = UIActivityIndicatorView(activityIndicatorStyle: .white)
            cell.imageAD.addSubview(activity)
            activity.center = cell.imageAD.center
            activity.startAnimating()
            cell.imageAD.sd_setImage(with: URL(string: strImageUrl))
            { (image, error, imageType, url) in
                activity.stopAnimating()
                activity.removeFromSuperview()
            }
            
            return cell
        }
        
    }
    
    func getMainOffer(arrOffers: [[String: Any]]) -> String {
        for dictOffer in arrOffers {
            let mainOffer = dictOffer["Main"] as? Bool ?? false
            if mainOffer {
                return "\(dictOffer["Discount"] as? String ?? "0")%"
            }
        }
        for dictOffer in arrOffers {
            return "\(dictOffer["Discount"] as? String ?? "0")%"
        }
        return "0%"
    }
}

//MARK: - Location Delegate Method
extension DashBoardVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        latitude = (location?.coordinate.latitude)!
        longitude = (location?.coordinate.longitude)!
        print("Latitude: ", (location?.coordinate.latitude)!,
              "\nLongitude: ", (location?.coordinate.longitude)!)
        if !isRestaurantListLoad {
            setMap()
            if !MyModel().isConnectedToInternet() {
                MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET,
                                          alertController: self)
            } else {
                //self.getRestaurantList(lat: latitude, long: longitude, keyword: "", km: 5, toKM: 0)
                self.getRestaurantSearchData(lat: "",
                                             long: "",
                                             km: 5,
                                             toKM: 0,
                                             strRestuarant: "",
                                             strCity: "")
            }
            isRestaurantListLoad = true
        }
        
        if startLocation == nil {
            startLocation = location
            endLocation = location
        } else {
            endLocation = location
        }
        
        let distance = self.getDistanceInMeter(origin: startLocation!,
                                                destination: endLocation!)
        if distance > 500 {
            startLocation = location
            //self.getRestaurantList(lat: latitude, long: longitude, keyword: "", km: 5, toKM: 0)
            self.getRestaurantSearchData(lat: "",
                                         long: "",
                                         km: 5,
                                         toKM: 0,
                                         strRestuarant: "",
                                         strCity: "")
        }
        
        currentLocation.position = CLLocationCoordinate2DMake(latitude, longitude)
        currentLocation.map = viewMap
    }
    
    func getDistanceInMeter(origin: CLLocation, destination: CLLocation) -> Double {
        let distanceInMeter = origin.distance(from: destination)
        return distanceInMeter
    }
}

//MARK: - GMSMapViewDelegate Delegate
extension DashBoardVC: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if marker != currentLocation {
            var markerData: NSDictionary?
            if let data = marker.userData! as? NSDictionary {
                markerData = data
            }
            
            locationMarker = marker
            infoWindow.removeFromSuperview()
            infoWindow = loadNib()
            
            guard let location = locationMarker?.position else {
                print("locationMarker is nil")
                return false
            }
            
            infoWindow.spotData = markerData
            infoWindow.delegate = self
            
            infoWindow.LabelRestaurantName.text = markerData!["RestaurantName"] as? String
            //infoWindow.labelDistance.text = "\(markerData!["Distance"] as?  NSNumber ?? 0) KM"
            
            let strDistance = Double(markerData!["Distance"] as? String ?? "0.0")
            infoWindow.labelDistance.text = "\(String(format: "%.2f", strDistance!)) KM"
            
            infoWindow.labelLocality.text = markerData!["Locality"] as? String
            
            let strOpenClose = "Open at \(markerData!["add_opning"] as? String ?? ""), Close at \(markerData!["closing_time"] as? String ?? "")"
            
            infoWindow.labelOpenClose.text = strOpenClose
            
            if let strRate = markerData!["Ratings"] as? String {
                let rate = Double(strRate)
                infoWindow.labelRate.text = "(\(strRate))"
                if rate! >= 1.00 && rate! < 2.00 {
                    infoWindow.imageRate1.image = #imageLiteral(resourceName: "ic_fill_star")
                    infoWindow.imageRate2.image = #imageLiteral(resourceName: "ic_empty_star")
                    infoWindow.imageRate3.image = #imageLiteral(resourceName: "ic_empty_star")
                    infoWindow.imageRate4.image = #imageLiteral(resourceName: "ic_empty_star")
                    infoWindow.imageRate5.image = #imageLiteral(resourceName: "ic_empty_star")
                } else if rate! >= 2.00 && rate! < 3.00 {
                    infoWindow.imageRate1.image = #imageLiteral(resourceName: "ic_fill_star")
                    infoWindow.imageRate2.image = #imageLiteral(resourceName: "ic_fill_star")
                    infoWindow.imageRate3.image = #imageLiteral(resourceName: "ic_empty_star")
                    infoWindow.imageRate4.image = #imageLiteral(resourceName: "ic_empty_star")
                    infoWindow.imageRate5.image = #imageLiteral(resourceName: "ic_empty_star")
                } else if rate! >= 3.00 && rate! < 4.00 {
                    infoWindow.imageRate1.image = #imageLiteral(resourceName: "ic_fill_star")
                    infoWindow.imageRate2.image = #imageLiteral(resourceName: "ic_fill_star")
                    infoWindow.imageRate3.image = #imageLiteral(resourceName: "ic_fill_star")
                    infoWindow.imageRate4.image = #imageLiteral(resourceName: "ic_empty_star")
                    infoWindow.imageRate5.image = #imageLiteral(resourceName: "ic_empty_star")
                } else if rate! >= 4.00 && rate! < 5 {
                    infoWindow.imageRate1.image = #imageLiteral(resourceName: "ic_fill_star")
                    infoWindow.imageRate2.image = #imageLiteral(resourceName: "ic_fill_star")
                    infoWindow.imageRate3.image = #imageLiteral(resourceName: "ic_fill_star")
                    infoWindow.imageRate4.image = #imageLiteral(resourceName: "ic_fill_star")
                    infoWindow.imageRate5.image = #imageLiteral(resourceName: "ic_empty_star")
                } else if rate! >= 5 {
                    infoWindow.imageRate1.image = #imageLiteral(resourceName: "ic_fill_star")
                    infoWindow.imageRate2.image = #imageLiteral(resourceName: "ic_fill_star")
                    infoWindow.imageRate3.image = #imageLiteral(resourceName: "ic_fill_star")
                    infoWindow.imageRate4.image = #imageLiteral(resourceName: "ic_fill_star")
                    infoWindow.imageRate5.image = #imageLiteral(resourceName: "ic_fill_star")
                } else {
                    infoWindow.imageRate1.image = #imageLiteral(resourceName: "ic_empty_star")
                    infoWindow.imageRate2.image = #imageLiteral(resourceName: "ic_empty_star")
                    infoWindow.imageRate3.image = #imageLiteral(resourceName: "ic_empty_star")
                    infoWindow.imageRate4.image = #imageLiteral(resourceName: "ic_empty_star")
                    infoWindow.imageRate5.image = #imageLiteral(resourceName: "ic_empty_star")
                }
            } else {
                infoWindow.imageRate1.image = #imageLiteral(resourceName: "ic_empty_star")
                infoWindow.imageRate2.image = #imageLiteral(resourceName: "ic_empty_star")
                infoWindow.imageRate3.image = #imageLiteral(resourceName: "ic_empty_star")
                infoWindow.imageRate4.image = #imageLiteral(resourceName: "ic_empty_star")
                infoWindow.imageRate5.image = #imageLiteral(resourceName: "ic_empty_star")
            }
            
            let arrOffers = markerData!["Offers"] as! [[String: Any]]
            if arrOffers.count > 0 {
                let dictOffers = arrOffers[0]
                if let dictDiscount = dictOffers["Kilometer"] as? [[String: Any]] {
                    infoWindow.labelDiscount.text = self.getMainOffer(arrOffers: dictDiscount)
                }
            } else {
                infoWindow.labelDiscount.text = ""
            }
            
            let strImageUrl = markerData!["LogoURL"] as? String ?? ""
            print(strImageUrl)
            let activity = UIActivityIndicatorView(activityIndicatorStyle: .white)
            infoWindow.imageRestaurant.addSubview(activity)
            activity.center = infoWindow.imageRestaurant.center
            activity.startAnimating()
            infoWindow.imageRestaurant.sd_setImage(with: URL(string: strImageUrl))
            { (image, error, imageType, url) in
                activity.stopAnimating()
                activity.removeFromSuperview()
            }
            
            infoWindow.center = mapView.projection.point(for: location)
            infoWindow.center.y = infoWindow.center.y - 110
            self.viewMap.addSubview(infoWindow)
        }
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if (locationMarker != nil){
            guard let location = locationMarker?.position else {
                print("locationMarker is nil")
                return
            }
            infoWindow.center = mapView.projection.point(for: location)
            infoWindow.center.y = infoWindow.center.y - 110
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        infoWindow.removeFromSuperview()
    }
}

//MARK: - MarkerView Delegate Method
extension DashBoardVC: MarkerViewDelegate {
    func didTapOnCkeckOffer(dictData: NSDictionary) {
        let offerVC = self.storyboard?.instantiateViewController(withIdentifier: "OfferListVC") as! OfferListVC
        offerVC.dictOffer = dictData as! [String : Any]
        self.navigationController?.pushViewController(offerVC, animated: true)
    }
}


//MARK: - API
extension DashBoardVC {
    func getRestaurantList(lat: Double, long: Double, keyword: String, km: Int, toKM: Int) {
        Define.APPDELEGATE.showLoadingView()
        let perameter: [String: Any] = ["Latitude": lat,
                                        "Longitude": long,
                                        "Keyword": keyword,
                                        "Kilometer": km,
                                        "ToKilometer": toKM]
        let strURL = Define.API_BASE_URL + Define.API_RESTAURANT_LIST
        print("Perameter: ", perameter, "\nURL: ", strURL)
        
        SwiftAPI().postMethod(stringURL: strURL,
                              parameters: perameter,
                              header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result, error) in
            if let error = error {
                Define.APPDELEGATE.hideLoadingView()
                if self.isRefresh {
                    self.refreshControl.endRefreshing()
                    self.isRefresh = false
                }
                print("Error: ", error)
                self.viewNoData.isHidden = false
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.getRestaurantList(lat: lat, long: long, keyword: keyword, km: km, toKM: toKM)
            } else {
                if self.isRefresh {
                    self.refreshControl.endRefreshing()
                    self.isRefresh = false
                }
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    self.viewMap.clear()
                    self.arrRestaurantList = result!["data"] as! [[String : Any]]
                    self.tableRestaurant.reloadData()
                    self.setMarker()
                    if self.isFromSearchCity {
                        self.isFromSearchCity = false
                        self.setSearchedCity()
                    }
                    if self.arrRestaurantList.count <= 0 {
                        self.viewNoData.isHidden = false
                    } else {
                        self.viewNoData.isHidden = true
                    }
                } else {
                    self.viewMap.clear()
                    self.arrRestaurantList.removeAll()
                    self.tableRestaurant.reloadData()
                    if self.isFromSearchCity {
                        self.isFromSearchCity = false
                        self.setSearchedCity()
                    }
                    self.viewNoData.isHidden = true
                }
            }
        }
    }
    
    func getRestaurantSearchData(lat: String, long: String, km: Int, toKM: Int, strRestuarant: String, strCity: String) {
        infoWindow.removeFromSuperview()
        Define.APPDELEGATE.showLoadingView()
        let perameter: [String: Any] = [
            "Latitude": latitude,
            "Longitude": longitude,
            "Kilometer": km,
            "ToKilometer": toKM,
            "Restaurant": strRestuarant,
            "City": strCity,
            "CityLatitude": lat,
            "CityLongitude": long
        ]
        let strURL = Define.API_BASE_URL + Define.API_RESTAURANT_DATA
        print("Perameter: ", perameter, "\nURL: ", strURL)
        
        SwiftAPI().postMethod(stringURL: strURL,
                              parameters: perameter,
                              header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result, error) in
            if let error = error {
                Define.APPDELEGATE.hideLoadingView()
                if self.isRefresh {
                    self.refreshControl.endRefreshing()
                    self.isRefresh = false
                }
                print("Error: ", error)
                self.viewNoData.isHidden = false
                //                MyModel().showAlertMessage(alertTitle: "Alert",
                //                                           alertMessage: Define.ALERT_SERVER,
                //                                           alertController: self)
                self.getRestaurantSearchData(lat: lat,
                                             long: long,
                                             km: km,
                                             toKM: toKM,
                                             strRestuarant: strRestuarant,
                                             strCity: strCity)
            } else {
                if self.isRefresh {
                    self.refreshControl.endRefreshing()
                    self.isRefresh = false
                }
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    self.viewMap.clear()
                    self.arrRestaurantList = result!["data"] as! [[String : Any]]
                    self.tableRestaurant.reloadData()
                    self.setMarker()
                    if self.isFromSearchCity {
                        self.isFromSearchCity = false
                        self.setSearchedCity()
                    }
                    if self.arrRestaurantList.count <= 0 {
                        self.viewNoData.isHidden = false
                    } else {
                        self.viewNoData.isHidden = true
                    }
                } else {
                    self.viewMap.clear()
                    self.arrRestaurantList.removeAll()
                    self.tableRestaurant.reloadData()
                    if self.isFromSearchCity {
                        self.isFromSearchCity = false
                        self.setSearchedCity()
                    }
                    self.viewNoData.isHidden = true
                }
            }
        }
    }
    
    func getAdvertisementListAPI() {
        let parameter: [String: Any] = ["Type": "Home Page"]
        let strURL = Define.API_BASE_URL + Define.API_ADVERTISEMENT_LIST
        print("Parameter: ", parameter, "\nURL: ", strURL)
        SwiftAPI().postGetActiveOfferMethod(stringURL: strURL,
                                            userID: Define.USERDEFAULT.value(forKey: "UserID") as! String,
                                            parameter: parameter,
                                            header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result, error) in
            if error != nil {
                print("Error: ", error!)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.getAdvertisementListAPI()
            } else {
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    self.viewAdvertiesment.isHidden = false
                    let dictData = result!["data"] as! [String: Any]
                    
                    let imageHeight = dictData["Height"] as! Float
                    let imageWidth = dictData["Width"] as! Float
                    print("Image Siza: ", imageHeight, imageWidth)
                    let deviceWidth = Float(self.view.frame.width)
                    let adImageHeight = (deviceWidth * imageHeight) / imageWidth
                    self.constraintImageBannerHeight.constant = CGFloat(adImageHeight)
                    let activity = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
                    self.imageBanner.addSubview(activity)
                    activity.center = self.imageBanner.center
                    activity.startAnimating()
                    self.imageBanner.sd_setImage(with: URL(string: dictData["AdvImaPath"] as! String))
                    { (image, error, imageType, url) in
                        activity.stopAnimating()
                        activity.removeFromSuperview()
                    }
                    
                    if let strMusicURL = dictData["Music"] as? String ?? nil {
                        print("Music URL: ", strMusicURL)
                        self.playAdvMusic(strURL: strMusicURL)
                    }
                    
                    
                } else {
                    self.viewAdvertiesment.removeFromSuperview()
                }
            }
        }
    }
    
    func getHealthTips() {
        let parameter: [String: Any] = ["UserID": Define.USERDEFAULT.value(forKey: "WalkerID")!]
        let strURL = Define.API_BASE_URL + Define.API_HEALTH_TIPS_LIST
        print("Parameter: ", parameter, "\nURL: ", strURL)
        
        SwiftAPI().postGetActiveOfferMethod(stringURL: strURL,
                                            userID: Define.USERDEFAULT.value(forKey: "UserID") as! String,
                                            parameter: parameter,
                                            header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result, error) in
            if error != nil {
                print("Error: ", error!)
                self.setHealthTipsSlider()
            } else {
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    self.arrHealthTips = result!["data"] as![[String: Any]]
                    self.setHealthTipsSlider()
                } else {
                    self.setHealthTipsSlider()
                }
            }
        }
    }
}

//["data": {
//    AdvImaPath = "http://clientsdemoarea.com/projects/walking/uploads/advertisement/nCgR5t7Uie2uHzjSlT0rDv1ks.jpg";
//    Description = "ABC text";
//    Height = 426;
//    Music = "http://clientsdemoarea.com/projects/walking/uploads/addmusic/music1.mp3";
//    Title = "WALK & GET DISCOUNT";
//    Width = 640;
//    }, "status_code": 200, "message": Advertisement List]

//MARK: - Alert Controller
extension DashBoardVC {
    
    func createSettingsAlertController(title: String, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default) { (UIAlertAction) in
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)! as URL, options: [:], completionHandler: nil)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func getPerStepCalories(weight: Double, stepSize: Double) -> Double{
        
        let footDistance = (Double(humanHeight) * stepSize) / 100
        
        let perMileCalories = ((weight * 2.20) * 0.57)
        
        let estimateStepPerMile = Int(1609.34 / footDistance)
        
         let perStepCalories = perMileCalories / Double(estimateStepPerMile)
        print ("Per Step Calories", perStepCalories)
        
        return perStepCalories
    }
}

//MARK: - Mucic Palyer
extension DashBoardVC {
    func playAdvMusic(strURL: String?) {
        
        guard let url = URL(string: strURL!) else {
            return
        }
        
        print("playing \(url)")
        
        let playerItem = AVPlayerItem(url: url)
        
        self.player = AVPlayer(playerItem:playerItem)
        player!.volume = 1.0
        player!.play()
    }
}

//MARK: - Set Page Controller
extension DashBoardVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollViewHealthTips.contentOffset.x / viewHealthTips.frame.width)
        pageControlHealthTips.currentPage = Int(pageIndex)
    }
    
    
    
    func setHealthTipsSlider() {
        scrollViewHealthTips.delegate = self
        slider = createSlider()
        setUpSlideScrollView(sliders: slider)
        pageControlHealthTips.numberOfPages = slider.count
        pageControlHealthTips.currentPage = 0
        viewHealthTips.bringSubview(toFront: pageControlHealthTips)
    }
    
    func setUpSlideScrollView(sliders: [ViewSlider]) {
        scrollViewHealthTips.frame = CGRect(x: viewHealthTips.frame.origin.x,
                                            y: viewHealthTips.frame.origin.y,
                                            width: viewHealthTips.frame.width,
                                            height: viewHealthTips.frame.height)
        scrollViewHealthTips.contentSize = CGSize(width: (viewHealthTips.frame.width * CGFloat(sliders.count)),
                                                  height: viewHealthTips.frame.height)
        scrollViewHealthTips.isPagingEnabled = true
        
        for i in 0 ..< sliders.count {
            slider[i].frame = CGRect(x: (viewHealthTips.frame.width * CGFloat(i)) + 8.0,
                                     y: 8,
                                     width: viewHealthTips.frame.width - 16.0,
                                     height: viewHealthTips.frame.height - 16.0)
            scrollViewHealthTips.addSubview(slider[i])
        }
    }
    
    func createSlider() -> [ViewSlider]{
        var arrSide = [ViewSlider]()
        
        if arrHealthTips.count > 0 {
            for data in arrHealthTips {
                let slider: ViewSlider = Bundle.main.loadNibNamed("ViewSlider", owner: self, options: nil)?.first as! ViewSlider
                slider.labelTips.text = data["Title"] as? String
                
                arrSide.append(slider)
            }
        } else {
            let slider: ViewSlider = Bundle.main.loadNibNamed("ViewSlider", owner: self, options: nil)?.first as! ViewSlider
            slider.labelTips.text = "No Health Tips"
            
            arrSide.append(slider)
        }
        return arrSide
    }
}

//MARK: - My DropDown Delegate
extension DashBoardVC: MyDropDownDelegate {
    func recievedSelectedValue(name: String) {
        if isFromKMS {
            self.buttonFromKMS.setTitle(name, for: .normal)
            fromKiloMeter = dictFilterKMS[name]!
            dropDownOBJ!.hideMyDropDown(sendButton: buttonFromKMS!)
        } else if isToKMS {
            self.buttonToKMS.setTitle(name, for: .normal)
            toKiloMeter = dictFilterKMS[name]!
            dropDownOBJ!.hideMyDropDown(sendButton: buttonToKMS!)
        }
        dropDownOBJ = nil
    }
}

//MARK: - Filter View Delegate
extension DashBoardVC: ViewFilterDelegate {
    func setFilterSelected(strValue: String) {
        buttonOptionFilter.setTitle(strValue, for: .normal)
        textSearch.text = nil
        if strValue == "City" {
            self.textSearch.isEnabled = false
            self.textSearch.placeholder = "Search By City"
            self.textSearch.removeTarget(nil, action: nil, for: .allEvents)
        } else {
            self.textSearch.isEnabled = true
            self.textSearch.placeholder = "Search By Restaurant"
            self.textSearch.delegate = self;
            self.textSearch.addTarget(self,
                                      action: #selector(self.handelSearch(_:)),
                                      for: .editingDidEnd)
            self.textSearch.returnKeyType = .search
        }
    }
}

//MARK: - TextField Delegate
extension DashBoardVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text!.count > 0  {
            textField.endEditing(true)
            buttonCloseFilter.isHidden = false
            self.getRestaurantList(lat: latitude, long: longitude, keyword: textField.text!, km: 5, toKM: 0)
        }
        return true
    }
}

//MARK: - Google Delegate Method
extension DashBoardVC: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        print("Name : \(place.name)")
        print("Latitude : \(place.coordinate.latitude)")
        print("Longitude : \(place.coordinate.longitude)")
        cityLatitude = place.coordinate.latitude
        cityLongitude = place.coordinate.longitude
        isFromSearchCity = true
        textSearch.text = place.name
        buttonCloseFilter.isHidden = false
        dismiss(animated: true, completion: nil)
//        self.getRestaurantList(lat: place.coordinate.latitude,
//                               long: place.coordinate.longitude,
//                               keyword: "",
//                               km: 10,
//                               toKM: 0)
        self.getRestaurantSearchData(lat: "\(cityLatitude)",
                                     long: "\(cityLongitude)",
                                     km: 10,
                                     toKM: 0,
                                     strRestuarant: "",
                                     strCity: place.name)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error : \(error.localizedDescription)")
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func setSearchedCity() {
        let camera = GMSCameraPosition.camera(withLatitude: cityLatitude,
                                              longitude: cityLongitude,
                                              zoom: 16)
        viewMap.animate(to: camera)
        viewMap.delegate = self
    }
}
