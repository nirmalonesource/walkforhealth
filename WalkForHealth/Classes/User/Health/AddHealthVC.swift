import UIKit

class AddHealthVC: UIViewController {

    //MARK: - Properteis
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var textWeight: FloatLabelTextField!
    @IBOutlet weak var textHeight: FloatLabelTextField!
    @IBOutlet weak var labelWeight: UILabel!
    @IBOutlet weak var labelHeight: UILabel!
    @IBOutlet weak var textAlegieticIssues: UITextField!
    @IBOutlet weak var labelAlegieticIssues: UILabel!
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var buttonSkip: UIButton!
    @IBOutlet weak var buttonBack: UIButton!
    
    @IBOutlet weak var buttonDaibetesYes: UIButton!
    @IBOutlet weak var buttonDaibetesNo: UIButton!
    @IBOutlet weak var buttonBloodPrasureYes: UIButton!
    @IBOutlet weak var buttonBloodPrasureNo: UIButton!
    
    private var isDaibetes = Bool()
    private var isBloodPressure = Bool()
    
    private var daibetes = 0
    private var bloodPressure = 0
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()

        textWeight.delegate = self
        textHeight.delegate = self
        textAlegieticIssues.delegate = self
        
        if Define.USERDEFAULT.bool(forKey: "isFromDashboard") {
            buttonBack.isUserInteractionEnabled = true
            buttonBack.isHidden = false
        } else {
            buttonBack.isUserInteractionEnabled = false
            buttonBack.isHidden = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        self.viewNavigation.backgroundColor = Define.APP_COLOR
        Design().setFixButton(button: buttonNext)
    }

    //MARK: - Button Method
    @IBAction func buttonBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonNext(_ sender: Any) {
        if textWeight.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Enter Weight",
                                      alertController: self)
        } else if textHeight.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Enter Height",
                                      alertController: self)
        } else if textAlegieticIssues.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Enter Alegietic Issues",
                                      alertController: self)
        } else {
            let dictData: [String: String] = ["Weight": textWeight.text!,
                                              "Height": textHeight.text!,
                                              "Daibetes": "\(daibetes)",
                                              "BloodPressure": "\(bloodPressure)",
                                              "AlegieticIssue": textAlegieticIssues.text!]
            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "AddHealthContinueVC") as! AddHealthContinueVC
            nextVC.dictHealthData = dictData
            self.navigationController?.pushViewController(nextVC, animated: true)
        }
    }
    
    @IBAction func buttonSkip(_ sender: Any) {
        if Define.USERDEFAULT.bool(forKey: "isFromDashboard") {
            dismiss(animated: true, completion: nil)
        } else {
            let storyBoard = UIStoryboard(name: "User", bundle: nil)
            let userVC = storyBoard.instantiateViewController(withIdentifier: "UserNC")
            present(userVC, animated: true, completion: {
                
            })
        }
    }
    
    @IBAction func buttonDaibetesYes(_ sender: Any) {
        if !isDaibetes {
            isDaibetes = true
            daibetes = 1
            buttonDaibetesYes.setImage(UIImage(named: "ic_check"), for: .normal)
            buttonDaibetesNo.setImage(UIImage(named: "ic_uncheck"), for: .normal)
        }
        
    }
    
    @IBAction func buttonDaibetesNo(_ sender: Any) {
        if isDaibetes {
            isDaibetes = false
            daibetes = 0
            buttonDaibetesYes.setImage(UIImage(named: "ic_uncheck"), for: .normal)
            buttonDaibetesNo.setImage(UIImage(named: "ic_check"), for: .normal)
        }
    }
    
    @IBAction func buttonBloodPrasureYes(_ sender: Any) {
        if !isBloodPressure {
            isBloodPressure = true
            bloodPressure = 1
            buttonBloodPrasureYes.setImage(UIImage(named: "ic_check"), for: .normal)
            buttonBloodPrasureNo.setImage(UIImage(named: "ic_uncheck"), for: .normal)
        }
    }
    
    @IBAction func buttonBloodPrasureNo(_ sender: Any) {
        if isBloodPressure {
            isBloodPressure = false
            bloodPressure = 0
            buttonBloodPrasureYes.setImage(UIImage(named: "ic_uncheck"), for: .normal)
            buttonBloodPrasureNo.setImage(UIImage(named: "ic_check"), for: .normal)
        }
    }
    
}

//MARK: - TextFeild Delegate Method
extension AddHealthVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == textWeight {
            labelWeight.backgroundColor = UIColor.white
        } else if textField == textHeight {
            labelHeight.backgroundColor = UIColor.white
        } else if textField == textAlegieticIssues {
            labelAlegieticIssues.backgroundColor = UIColor.white
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == textWeight {
            labelWeight.backgroundColor = Define.LABEL_COLOR
        } else if textField == textHeight {
            labelHeight.backgroundColor = Define.LABEL_COLOR
        } else if textField == textAlegieticIssues {
            labelAlegieticIssues.backgroundColor = Define.LABEL_COLOR
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textWeight {
            let maxLength = 3
            let currentString: NSString = textWeight.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        } else if textField == textHeight {
            let maxLength = 3
            let currentString: NSString = textHeight.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        return true
    }
}

