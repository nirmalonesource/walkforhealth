import UIKit

class SuccessfullVC: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageSuccefull: UIImageView!
    @IBOutlet weak var labelRestaurantName: UILabel!
    @IBOutlet weak var LabelDistance: UILabel!
    @IBOutlet weak var labelPromoCode: UILabel!
    
    var dictRestaurant = [String: Any]()
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        self.viewNavigation.backgroundColor = Define.APP_COLOR
        Design().setLabel(label: labelRestaurantName)
        labelRestaurantName.text = dictRestaurant["RestaurantName"] as? String
        LabelDistance.text = "\(dictRestaurant["Distance"] as! NSNumber)KM"
    }
    

    //MARK: - Default Method
    @IBAction func buttonBack(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}
