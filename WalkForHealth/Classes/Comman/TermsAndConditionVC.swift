import UIKit

class TermsAndConditionVC: UIViewController {
    //MARK: - Properties
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var labelCondition: UILabel!
    
    //MARK: - Default Mrthod
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        viewNavigation.backgroundColor = Define.APP_COLOR
        labelCondition.textColor = Define.APP_COLOR
    }
    
    //MARK: - Button Mrthod
    @IBAction func buttonBack(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
