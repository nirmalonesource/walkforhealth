import UIKit

class AboutUsVC: UIViewController {
    //MARK: - properties
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var labelAboutUs: UILabel!
    
    //MARK: - Default Mehtod
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        self.viewNavigation.backgroundColor = Define.APP_COLOR
        self.labelAboutUs.textColor = Define.APP_COLOR
    }
    
    //MARK: - Button Method
    @IBAction func buttonBack(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
