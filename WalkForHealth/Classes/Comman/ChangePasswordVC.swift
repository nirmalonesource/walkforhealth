import UIKit

class ChangePasswordVC: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var textOldPassword: FloatLabelTextField!
    @IBOutlet weak var labelOldPassword: UILabel!
    @IBOutlet weak var textNewPassword: FloatLabelTextField!
    @IBOutlet weak var labelNewPassword: UILabel!
    @IBOutlet weak var textConfirmPassword: FloatLabelTextField!
    @IBOutlet weak var labelConfirmPassword: UILabel!
    @IBOutlet weak var buttonSubmit: UIButton!
    
    //MARK: - Default Method.
    override func viewDidLoad() {
        super.viewDidLoad()

        textOldPassword.delegate = self
        textNewPassword.delegate = self
        textConfirmPassword.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        self.viewNavigation.backgroundColor = Define.APP_COLOR
        Design().setButton(button: buttonSubmit)
    }
    
    //MARK: - Button Method
    @IBAction func buttonBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonSubmit(_ sender: Any) {
        if textOldPassword.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Enter Old Password", alertController: self)
        } else if textNewPassword.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Enter New Password", alertController: self)
        } else if textConfirmPassword.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Enter Confirm Password", alertController: self)
        } else if textNewPassword.text! != textConfirmPassword.text! {
            MyModel().showTostMessage(alertMessage: "Confirm Password Not Match", alertController: self)
        } else {
            self.changePasswordAPI()
        }
    }
    
}

//MARK: - Textfield Delegate Method
extension ChangePasswordVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == textOldPassword {
            labelOldPassword.backgroundColor = Define.APP_COLOR
        } else if textField == textNewPassword {
            labelNewPassword.backgroundColor = Define.APP_COLOR
        } else if textField == textConfirmPassword {
            labelConfirmPassword.backgroundColor = Define.APP_COLOR
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == textOldPassword {
            labelOldPassword.backgroundColor = Define.LABEL_DARK_COLOR
        } else if textField == textNewPassword {
            labelNewPassword.backgroundColor = Define.LABEL_DARK_COLOR
        } else if textField == textConfirmPassword {
            labelConfirmPassword.backgroundColor = Define.LABEL_DARK_COLOR
        }
    }
}

//MARK: - API
extension ChangePasswordVC {
    func changePasswordAPI() {
        Define.APPDELEGATE.showLoadingView()
        let parameter: [String: Any] = ["OldPassword": textOldPassword.text!,
                                        "NewPassword": textNewPassword.text!,
                                        "ConfirmNewPassword": textConfirmPassword.text!]
        let strURL = Define.API_BASE_URL + Define.API_CHANGE_PASSWORD
        print("Parameter: ", parameter, "\nURL: ", strURL)
        SwiftAPI().postChangePasswordMethod(stringURL: strURL,
                                            userID: Define.USERDEFAULT.value(forKey: "UserID") as! String,
                                            parameter: parameter,
                                            header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result, error) in
            if let error = error {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.changePasswordAPI()
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    Define.USERDEFAULT.set(self.textNewPassword.text!, forKey: "Password")
                    self.textOldPassword.text = nil
                    self.textNewPassword.text = nil
                    self.textConfirmPassword.text = nil
                    MyModel().showTostMessage(alertMessage: result!["message"] as! String,
                                              alertController: self)
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
}
