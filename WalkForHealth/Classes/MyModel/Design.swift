import UIKit

class Design: NSObject {
    //MARK: Button
    func setFixButton(button: UIButton) {
        button.backgroundColor = Define.BUTTON_COLOR
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = button.frame.size.height / 2
        button.layer.masksToBounds = true
    }
    
    func setButton(button: UIButton) {
        button.backgroundColor = Define.APP_COLOR
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = button.frame.size.height / 2
        button.layer.masksToBounds = true
    }
    
    func setWhiteTestButton(button: UIButton) {
        
    }
    
    func setCustomButton(button: UIButton) {
        button.layer.cornerRadius = button.frame.size.height / 2
        button.layer.masksToBounds = true
        button.layer.shadowColor = UIColor.darkGray.cgColor
        button.layer.shadowRadius = 2
        button.layer.shadowOpacity = 0.5
        button.layer.shadowOffset = CGSize(width: 0, height: 2)
    }
    
    //MARK: View
    func viewWithShadow(view: UIView){
        view.clipsToBounds = false
        view.layer.cornerRadius = 8
        view.layer.shadowRadius = 2
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 1, height: 1)
    }
    
    func viewSerchWithShadow(view: UIView)  {
        view.clipsToBounds = false
        view.layer.cornerRadius = 8
        view.layer.shadowRadius = 5
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 1, height: 1)
    }
    
    //MARK: Profile Image
    func setProfileImage(image: UIImageView) {
        image.layer.borderWidth = 2
        image.layer.borderColor = UIColor.white.cgColor
        image.layer.cornerRadius = image.frame.size.height / 2
        image.layer.masksToBounds = true
    }
    
    //MARK: - Set Label
    func setLabel(label: UILabel) {
        label.textColor = Define.APP_COLOR
    }
}

class CommenSimpleButton: UIButton {
    override func awakeFromNib() {
        self.backgroundColor = Define.APP_COLOR
        self.layer.cornerRadius = self.frame.height / 2
    }
}
