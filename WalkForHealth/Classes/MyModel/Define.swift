import UIKit
import Foundation

class Define {
    //MARK: Object
    static let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate
    static let USERDEFAULT = UserDefaults.standard
    static let PLACEHOLDER_USER_IMAGE = #imageLiteral(resourceName: "ic_temp_register_avatar")
    
    //MARK: App Color
    // Hex Color: #3498db
    static let APP_COLOR = UIColor(red: 18/255.0, green: 179/255.0, blue: 207/255.0, alpha: 1)
    //Hex Color: #09609a
    static let BUTTON_COLOR = UIColor(red: 10/255.0, green: 140/255.0, blue: 162/255.0, alpha: 1)
    static let LABEL_COLOR = UIColor(red: 10/255.0, green: 140/255.0, blue: 162/255.0, alpha: 1)
    static let LABEL_DARK_COLOR = UIColor(red: 82/255.0, green: 82/255.0, blue: 82/255.0, alpha: 1)
    
    //MARK: Alert Text
    static let ALERT_INTERNET = "Check your internet connection"
    static let ALERT_SERVER = "Could not connect to the server"
    
    //Font
    //Myriad Hebrew Font names: ["MyriadHebrew-Bold", "MyriadHebrew-BoldIt", "MyriadHebrew-Regular", "MyriadHebrew-It"]
    //MARK: - Header Token.
    static let TOKEN_FIX_AUTHORIZATION = "Basic V0FMS0lORy1BRE1JTjpBUElAV0FMS0lORyEjJFdFQiQ="
    static let TOKEN_X_WALKING_API_KEY = "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss"
    
    //MARK: - URL
    static let API_BASE_URL = "http://clientsdemoarea.com/projects/walking/api/version_1/web_services/"
    static let SHARE_LINK_URL = "http://clientsdemoarea.com/projects/walking/api/version_1/"
    
    
    //MARK: - API LIST
    static let API_LOGIN = "login"
    static let API_LOGOUT = "logout"
    static let API_FORGOT_PASSWORD = "forgot_password"
    static let API_SOCIAL_LOGIN = "social_login"
    static let API_REGISTER_WALKER = "register_walker"
    static let API_OTP = "confirm_otp"
    static let API_RESTAURANT_REGISTER = "register_restaurant"
    static let API_CHANGE_PASSWORD = "change_password"
    static let API_ADD_OFFER = "add_edit_offer"
    static let API_RESTAURANT_LIST = "restaurant"
    static let API_RESTAURANT_ACTIVE_OFFER = "active_offer_list"
    static let API_ACTIVE_USER = "active_user_list"
    static let API_ADD_WALKER_HEALTH = "add_walker_health"
    static let API_UPDATE_WALKER = "update_walker"
    static let API_UPDATE_RESTAURANT = "update_restaurant"
    static let API_ADD_WALKER_OFFER = "add_walker_offer"
    static let API_UPDATE_WALKER_OFFER = "update_walker_offer"
    static let API_TERMINATE_WALKER_OFFER = "terminate_walker_offer"
    static let API_USER_HISTORY = "user_history"
    static let API_USER_OFFER_DETAIL = "user_offer_details"
    static let API_USER_OFFER_HISTORY = "user_offer_history"
    static let API_CONFIRM_PROMOCODE = "confirm_promocode"
    static let API_UPDATE_RESTAURANT_LONGITUDE = "update_restaurant_longitude"
    static let API_ADD_RATTING = "add_ratings"
    static let API_ADVERTISEMENT_LIST = "advertisement_list"
    static let API_CONFIRM_WALKER_OFFER = "confirm_walker_offer"
    static let API_HEALTH_TIPS_LIST = "healthtips_list"
    static let API_GET_OFFER_DETAILS = "get_offer_details"
    static let API_RESTAURANT_DETAILS = "restaurant_details"
    static let API_CHECK_EMAIL_MOBILE = "is_email_or_mobile_available_checked"
    static let API_RESTAURANT_DATA = "restaurant_data"
}
